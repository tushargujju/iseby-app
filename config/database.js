const mysql = require('mysql');

//Server
        //host     : 'us-cdbr-iron-east-05.cleardb.net',
        //user     : 'beab5a368d1c1e',
       // password : 'f0d6931c',
        //database : 'heroku_7d953a128177597'

//local
var connection = mysql.createConnection(
    {
         /*host     : 'us-cdbr-iron-east-05.cleardb.net',
         user     : 'beab5a368d1c1e',
         password : 'f0d6931c',
         database : 'heroku_7d953a128177597'*/
     
        //  Local
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'iseby'
    });

connection.connect(function (error) {
    if (error) {
        throw error;
    } else {
        console.log("Database connected");
    }
});

setInterval(function () {
    connection.query('SELECT 1');
}, 5000);

module.exports = {

    connection: connection,

};


/*

CLEARDB_DATABASE_URL

mysql://beab5a368d1c1e:f0d6931c@us-cdbr-iron-east-05.cleardb.net/heroku_7d953a128177597?reconnect=true


CLOUDINARY_URL

cloudinary://733123771549582:BE5H7rzJGfO8gfW5tIQfBe4wHns@hnjdkldzg


DATABASE_URL

'mysql://beab5a368d1c1e:f0d6931c@us-cdbr-iron-east-05.cleardb.net/heroku_7d953a128177597?reconnect=true'
*/
