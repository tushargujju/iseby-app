const express = require('express');
const bodyparser = require('body-parser');
const userCtrl = require('./controllers/users/user.ctrl');
const catCtrl = require('./controllers/categories/categories.ctrl');
const subCatCtrl = require('./controllers/subcategories/subcategories.ctrl');
const prodCtrl = require('./controllers/product/product.ctrl');
const sharedCtrl = require('./controllers/shared/shared.ctrl');
const orderCtrl = require('./controllers/order/order.ctrl');
const scannedCtrl = require('./controllers/scanned/scanned.ctrl');
const subscription = require('./controllers/subscription/subscription.ctrl');
const subscription_details = require('./controllers/subscription_details/subscription_details.ctrl');
const wishlistCntrl = require('./controllers/wishlist/wishlist.ctrl');
const remindersCntrl = require('./controllers/reminders/reminders.ctrl');
const superAdminControl = require('./controllers/superadmin/superadmin.ctrl');
const sellItem = require('./controllers/soldItems/sellItem.ctrl');
const cart = require('./controllers/cart/cart.ctrl');
const payment = require('./controllers/payment/payment.ctrl');
const paymentGetway = require('./controllers/PaymentGetway/PaymentGetway.ctrl');
const gst = require('./controllers/gst/gst.ctrl');
const bodyParser = require('body-parser');

const cors = require('cors')

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
var multer  = require('multer');

require('./controllers/users/user.routes')(app);
require('./controllers/categories/categories.routes')(app);
require('./controllers/subcategories/subcategories.routes')(app);
require('./controllers/product/product.routes')(app);
require('./controllers/shared/shared.routes')(app);
require('./controllers/order/order.routes')(app);
require('./controllers/scanned/scanned.routes')(app);
require('./controllers/subscription/subscription.routes')(app);
require('./controllers/subscription_details/subscription_details.routes')(app);
require('./controllers/wishlist/wishlist.routes')(app);
require('./controllers/reminders/reminders.routes')(app);
require('./controllers/superadmin/superadmin.routes')(app);
require('./controllers/soldItems/sellItem.routes')(app);
require('./controllers/cart/cart.routes')(app);
require('./controllers/payment/payment.routes')(app);
require('./controllers/PaymentGetway/PaymentGetway.routes')(app);
require('./controllers/gst/gst.routes')(app);

app.set('port', (process.env.PORT || 5041));


app.listen(app.get('port'), function(){

  console.log('ISEBY app listening on port !'+app.get('port'));

});
