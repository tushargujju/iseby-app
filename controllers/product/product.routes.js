const prodCtrl = require('./product.ctrl');
const express = require('express');

const router = express.Router();

var multer  = require('multer')
var upload = multer({ dest: 'uploads/temp/' })


module.exports = function(app){

  // router.get(
  //   '/qrcode/:product_id',
  //   prodCtrl.getProductBarcode
  // );

  router.get(
    '/search/:keyword',
    prodCtrl.doSearchProducts
  );

  router.get(
    '/user/products/:user_id',
    prodCtrl.getUsersProductsList
  );

  router.get(
    '/:product_id',
    prodCtrl.getProductDetails
  );

  router.get(
    '/images/:product_id',
    prodCtrl.getProductImages
  );

  router.get(
    '/store/list',
    prodCtrl.getProductList
  );

  router.post(
    '/',
    upload.any(),
    prodCtrl.create
  );

    router.put(
        '/',
        //upload.any(),
        prodCtrl.updateProduct
    );

    router.get(
        '/purchased/product',
        prodCtrl.getAlPurchasedProduct
    );
    router.get(
        '/purchasedProductByBuyer/:user_id',
        prodCtrl.getPurchasedProductByUser
    );

    router.get(
      '/getProductsDetailInfo',prodCtrl.getAllProductWithAllInformation);

     app.use('/product',router);




     
};
