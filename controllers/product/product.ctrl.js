const connection = require('../../config/database').connection;
const subscription = require("../subscription/subscription.ctrl");
const globalFunction = require("../../gloalFunctions/global");
var uniqid = require('uniqid');
var fs = require('fs');
var QRCode = require('qrcode')
var cloudinary = require('cloudinary');

//Config iseby-server-api
/*cloudinary.config({
  cloud_name: 'hnjdkldzg',
  api_key: '733123771549582',
  api_secret: 'BE5H7rzJGfO8gfW5tIQfBe4wHns'
});*/
//config iseby
cloudinary.config({  
    cloud_name: 'isbey-sample',
    api_key: '979539496732217',
    api_secret: 'IWowFCLgR6ZCODUldPc_3-sn04A'
});

function checkMendatoryFields(inputObject) {
    var allKeys = Object.keys(inputObject);
    var output = {
        isError: 0,
        message: ""
    };
    for (var counter = 0; counter < allKeys.length; counter++) {
        var currentKey = allKeys[counter];
        if (isNullOrUndefined(inputObject[currentKey])) {
            output.isError = 1;
            output.message = "Please specify ," + currentKey + ".It should not be null or undefined";
            break;
        } else if (inputObject[currentKey] == "") {
            output.isError = 1;
            output.message = "Please specify ," + currentKey + ".It should not be empty";
            break;
        }
    }
    return output;}

var isNullOrUndefined = function(val) {
    return val === undefined || val == null ? 1 : 0;}


var updateProduct = function(req, resp) {
    /*
        Author : Vinay Jadhav
        Information : This will update a product  . This API is partially developed
     */

    var xBody = req.body;
    var name = xBody.name;
    var description = xBody.description;
    var seller_id = xBody.seller_id;
    var quantity = xBody.quantity;
    var price = xBody.price;
    var barcode_color = xBody.barcode_color;
    var category_id = xBody.category_id;
    var sub_category = xBody.sub_category;
    var productId = xBody.productId;
    var productCode = xBody.productCode;
    var isActive = xBody.isActive;
    var modified_at = Math.round(+new Date() / 1000);
    var isShippingIncluded = xBody.isShippingIncluded;
    var shippingCharges = xBody.shippingCharges;
    var addGst = xBody.addGst;


    var responseObj = {
        status: 0,
        message: "",
        data: [],
    };
    var checkCompulsoryFields = {
        name: name,
        description: description,
        seller_id: seller_id,
        quantity: quantity,
        price: price,
        barcode_color: barcode_color,
        category_id: category_id,
        sub_category: sub_category,
        productId: productId,
        productCode: productCode,
        isShippingIncluded: isShippingIncluded,
        shippingCharges: shippingCharges,
        addGst: addGst
    };

    var output = checkMendatoryFields(checkCompulsoryFields);
    console.log(output);
    if (output.isError == 0) {
        subscription.checkSubscriptionOfUser({
            sellerId: seller_id
        }, function(response) {
            if (response.status) {
                if (response.data.isExpired == 0) {

                    var inputData = [productCode, name, description, seller_id, quantity, price, barcode_color,
                        category_id, sub_category, modified_at, isActive, productId
                    ];
                    var sqlString = " UPDATE `tbl_products` SET  `product_code`= '" + productCode + "',`name`= '" + name + "'," +
                        "`description`= '" + description + "',`seller_id`= " + seller_id + "," +
                        " `quantity`= " + quantity + ",`price`= " + price + ",`barcode_color`= '" + barcode_color + "'," +
                        " `category_id`= " + category_id + ",`sub_category`= " + sub_category + ",`modified_at`= '" + modified_at + "'" +
                        " ,`isActive`=" + isActive + " ,`isShippingIncluded` = " + isShippingIncluded + " ,shippingCharges=" + isShippingIncluded + ",addGst = " + addGst + " WHERE `id`=" + productId;
                    console.log(sqlString);
                    connection.query(sqlString, function(error, result) {
                        if (!error) {
                            if (result.affectedRows > 0) {
                                var mProductID = productId;
                                if (req.files != undefined) {
                                    req.files.forEach(function(element, index, array) {

                                    });
                                }

                                var qrcodeFile = __dirname + "/../../uploads/productqrcode/" + productId + ".png";
                                var opts = {
                                    errorCorrectionLevel: 'H',
                                    color: {
                                        dark: barcode_color, // Blue dots
                                        light: '#0000' // Transparent background
                                    },
                                    type: 'image/jpeg',
                                    rendererOpts: {
                                        quality: 0.3
                                    }
                                };

                                QRCode.toDataURL(productId, opts, function(qrCodeError, qrCodeUrl) {
                                    if (!qrCodeError) {
                                        cloudinary.v2.uploader.upload(qrCodeUrl, function(cloudinaryError, cloudinaryResult) {
                                            if (!cloudinaryError) {
                                                var tableMediaQuery = "\tUPDATE `tbl_media` SET `file_name`='" + cloudinaryResult + "'," +
                                                    "`product_id`=" + productId + ",`type`=2," +
                                                    "`modified_at`='" + modified_at + "' WHERE `id`=" + tblMediaId;

                                                connection.query(tableMediaQuery, function(mediaUPdateError, mediaResult) {
                                                    if (!mediaUPdateError) {
                                                        if (mediaResult.affectedRows > 0) {
                                                            responseObj['status'] = 1;
                                                            responseObj['data'] = [];
                                                            responseObj['message'] = mediaResult;
                                                            resp.status(200).json(responseObj);
                                                        } else {
                                                            responseObj['status'] = 0;
                                                            responseObj['data'] = [];
                                                            responseObj['message'] = "Sorry , table media data is not updated";
                                                            resp.status(200).json(responseObj);
                                                        }
                                                    } else {
                                                        responseObj['status'] = 0;
                                                        responseObj['data'] = [];
                                                        responseObj['message'] = mediaUPdateError;
                                                        resp.status(200).json(responseObj);
                                                    }
                                                });
                                            } else {
                                                responseObj['status'] = 0;
                                                responseObj['data'] = [];
                                                responseObj['message'] = cloudinaryError;
                                                resp.status(200).json(responseObj);
                                            }
                                        });
                                    } else {
                                        responseObj['status'] = 0;
                                        responseObj['data'] = [];
                                        responseObj['message'] = qrCodeError;
                                        resp.status(200).json(responseObj);
                                    }
                                });
                            } else {
                                responseObj['status'] = 0;
                                responseObj['data'] = [];
                                responseObj['message'] = "Sorry no records were updated";
                                resp.status(400).json(responseObj);
                            }
                        } else {
                            responseObj['status'] = 0;
                            responseObj['data'] = [];
                            responseObj['message'] = error;
                            resp.status(400).json(responseObj);
                        }
                    });
                } else {
                    responseObj['status'] = 0;
                    responseObj['product_id'] = 0;
                    responseObj['message'] = response.message;
                    res.status(400).json(responseObj);
                }
            } else {
                responseObj['status'] = 0;
                responseObj['product_id'] = 0;
                responseObj['message'] = response.message;
                res.status(401).json(responseObj);
            }
        });

    } else {
        responseObj.message = output.message;
        resp.status(400).json(responseObj);
    }
}

var create = function(req, res, next) 
{
    /*
        Author : Vinay Jadhav
        Information : This API will create a product . WIll check for seller whether subscription is still exists
     */

    var xBody = req.body;
    var name = xBody.name;
    var description = xBody.description;
    var seller_id = xBody.seller_id;
    var quantity = xBody.quantity;
    var price = xBody.price;
    var discount_per = xBody.discount_per;
    var barcode_color = xBody.barcode_color;
    var category_id = xBody.category_id;
    var sub_category = xBody.sub_category_id;
    var created_at = Math.round(+new Date() / 1000);;
    var modified_at = Math.round(+new Date() / 1000);;
    var isActive=1;
    var sgstid = xBody.addGst;
    var isShippingIncluded = xBody.isShippingIncluded;
    var shippingChargesCity = xBody.shippingChargesCity;   
    var shippingChargesState = xBody.shippingChargesState;   
    var shippingChargesNational = xBody.shippingChargesNational;   
    var buy_point = xBody.buyPoints;   
    var refer_point = xBody.referPoints;   
    var tax_type = xBody.taxType;   
    var tax_cat = xBody.taxCategory;   
    var tax_per = xBody.taxPercentage;
    var redeem_points = xBody.redeem_points;
    var redeem_discount = xBody.redeem_discount;

	
    var responseObj = {};
    subscription.checkSubscriptionOfUser({
        sellerId: seller_id
    }, function(response) {
        if (response.status) 
        {
            if (response.data.isExpired == 0) {
                //If subscription is not expired
                var productID = uniqid.time().toUpperCase();


                var add_product = "INSERT INTO tbl_products (product_code, name, description, seller_id, quantity, price, discount_per, barcode_color, category_id, sub_category, created_at, modified_at, isActive, sgstid, city_shipping, city_charges, out_shipping, out_charges, state_shipping, state_charges, buy_point, refer_point, tax_type, tax_cat, tax_per, redeem_point, redeem_discount) VALUES ('" + productID + "', '" + name + "', '" + description + "', '" + seller_id + "', '" + quantity + "', '" + price + "', '"+ discount_per + "', '" + barcode_color + "', '" + category_id + "', '" + sub_category + "', '" + created_at + "', '" + modified_at + "' , '"+isActive+"', '"+sgstid+"', '" + isShippingIncluded + "' , '"+ shippingChargesCity + "' , '"+ isShippingIncluded + "' , '"+ shippingChargesState +"' , '"+ isShippingIncluded + "' , '"   + shippingChargesNational + "' , '"   + buy_point + "' , '"   + refer_point + "' , '"   + tax_type + "' , '"   + tax_cat + "' , '"   + tax_per + "', '" + redeem_points+ "', '" +redeem_discount+ "')";
				
				
					

                connection.query(add_product, function(error, results, fields) {

                    if (error) throw error;
                    //console.log(results[0]['COUNT']);

                    var mProductID = results.insertId;

                    //Create QR Code
                    var qrcodeFile = __dirname + "/../../uploads/productqrcode/" + productID + ".png";

                    var opts = {
                        errorCorrectionLevel: 'H',
                        color: {
                            dark: barcode_color, // Blue dots
                            light: '#0000' // Transparent background
                        },
                        type: 'image/jpeg',
                        rendererOpts: {
                            quality: 0.3
                        }
                    }

                    QRCode.toDataURL(productID, opts, function(err, url) {
                        if (err) throw err

                        cloudinary.v2.uploader.upload(url, function(error, result) {
                            console.log(result);

                            if (error) throw error;

                            //Add File Details to Media Table
                            var add_file_to_media = "INSERT INTO tbl_media (file_name, product_id, type, created_at, modified_at) VALUES ('" + result.url + "', '" + mProductID + "', 2, '" + created_at + "', '" + modified_at + "')";

                            connection.query(add_file_to_media, function(error, results, fields) {
                                //File Added

                            });

                        });

                    })

                    //Save Multiple Product Images
                    req.files.forEach(function(element, index, array) {


                        var file_name = productID + "_" + index + "_" + Math.round(+new Date() / 1000) + ".jpg";
                        var newPath = __dirname + "/../../uploads/product/" + file_name;

                        console.log("File Upload Path : " + newPath);

                        cloudinary.v2.uploader.upload(element.path,
                            function(error, result) {
                                console.log(result);

                                if (error) throw error;


                                //Add File Details to Media Table
                                var add_file_to_media = "INSERT INTO tbl_media (file_name, product_id, type, created_at, modified_at) VALUES ('" + result.url + "', '" + mProductID + "', 1, '" + created_at + "', '" + modified_at + "')";

                                connection.query(add_file_to_media, function(error, results, fields) {
                                    //File Added

                                });


                            });

                        //Delete Temp files
                        const path = require('path');
                        //     fs.readdir(__dirname + "/../../uploads/temp/", (err, files) => {
                        //         //if (err) throw err;
                        //
                        //         for(const file of files
                        // )
                        //     {
                        //         fs.unlink(path.join(__dirname + "/../../uploads/temp/", file), err = > {
                        //             //if (err) throw err;
                        //         }
                        //     )
                        //         ;
                        //     }
                        // })
                        ;

                    });

                    responseObj['status'] = 1;
                    responseObj['product_id'] = mProductID;
                    responseObj['message'] = "Product added successfully.";
                    res.status(200).json(responseObj);

                });

            } else {
                responseObj['status'] = 0;
                responseObj['product_id'] = 0;
                responseObj['message'] = response.message;
                res.status(200).json(responseObj);
            }
        } else {
            responseObj['status'] = 0;
            responseObj['product_id'] = 0;
            responseObj['message'] = response.message;
            res.status(401).json(responseObj);
        }
    });


 //Generate Unique Product ID


}

var getProductList = function(req, res, next) {

    console.log(req.body);

    var xBody = req.body;

    var getProductListQuery = "SELECT * FROM tbl_products";
    connection.query(getProductListQuery, function(error, results, fields) {

        var responseObj = {};

        //if(error)throw error;
        responseObj['status'] = 1;
        responseObj['message'] = 'Products fetched successfully';
        responseObj['data'] = results;

        res.status(200).json(responseObj);


    });

}

var getProductDetails = function(req, res, next) {

    console.log(req.body);

    var product_id = req.params.product_id;

    var getProductDetailsQuery = "SELECT * FROM tbl_products WHERE id='" + product_id + "'";
    connection.query(getProductDetailsQuery, function(error, results, fields) {

        var responseObj = {};

        //if(error)throw error;
        responseObj['status'] = 1;
        responseObj['message'] = 'Product details fetched successfully';
        responseObj['data'] = results[0];

        res.status(200).json(responseObj);


    });

}

var getProductImages = function(req, res, next) {

    console.log(req.body);

    var product_id = req.params.product_id;

    var getProductDetailsQuery = "SELECT * FROM tbl_media WHERE product_id='" + product_id + "'";
    connection.query(getProductDetailsQuery, function(error, results, fields) {

        var responseObj = {};

        //if(error)throw error;
        responseObj['status'] = 1;
        responseObj['message'] = 'Product images fetched successfully';
        responseObj['data'] = results;

        res.status(200).json(responseObj);


    });

}


var getUsersProductsList = function(req, res, next) {

    var user_id = req.params.user_id;

    var getProductListQuery = "SELECT * FROM tbl_products WHERE seller_id = '" + user_id + "' AND isActive=1";
    connection.query(getProductListQuery, function(error, results, fields) {

        var responseObj = {};

        if (error) throw error;

        responseObj['status'] = 1;
        responseObj['message'] = 'Products fetched successfully';
        responseObj['data'] = results;

        res.status(200).json(responseObj);


    });

}

var getAllProductWithAllInformation = function(callback) {
    var response = {
        messsage: "",
        status: 0,
        data: []
    };
    var query = "SELECT prod.`created_at` ,prod.`modified_at` , prod.`name` , prod.`description`,prod.`seller_id`," +
        "prod.`quantity`,prod.`price`,prod.`barcode_color`, prod.`category_id` , prod.`sub_category` , " +
        "u.`id` as `userId` , u.`name` as userName , u.`email` as userEmail , u.`mobile`, " +
        "cat.`name` as `categoryName` FROM `tbl_products` prod INNER JOIN `tbl_seller_info` si ON " +
        "prod.`seller_id`=si.`id` INNER JOIN `tbl_users` u ON u.`id` = si.`user_id` INNER JOIN" +
        " `tbl_categories` cat ON prod.`category_id` = cat.`id`";
    connection.query(query, function(err, result) {
        if (!err) {
            if (result.length > 0) {
                response.message = "Records found";
                response.data = result;
                response.status = 1;
                callback(response);
            } else {
                response.message = "No records found";
                callback(response);
            }
        } else {
            response.message = err;
            callback(response);
        }
    });
}

var doSearchProducts = function(req, res, next) {

    var keyword = req.params.keyword;

    var getProductListQuery = "SELECT tbl_products.*, tbl_seller_info.shop_name FROM tbl_products, tbl_seller_info WHERE tbl_products.product_code LIKE '%" + keyword + "%' AND tbl_products.isActive='1' AND tbl_seller_info.user_id=tbl_products.seller_id";
    connection.query(getProductListQuery, function(error, results, fields) {

        var responseObj = {};

        if (error) throw error;

        responseObj['status'] = 1;
        responseObj['message'] = 'Products fetched successfully';
        responseObj['data'] = results;

        res.status(200).json(responseObj);


    });

}

var deactivateProduct = function(inputData, callback) {

    var responseObj = {
        message: "",
        status: 0,
        data: []
    };
    if (inputData.isActive !== undefined) {
        var isActive = inputData.isActive;
        var productId = inputData.productId;
        if (inputData.productId != undefined) {
            var updateProductStatus = "UPDATE `tbl_products` SET `isActive`= " + isActive + " WHERE  id=" + productId;
            connection.query(updateProductStatus, function(error, results, fields) {
                if (!error) {
                    if (results.affectedRows > 0) {
                        responseObj.message = "Status activated success"
                        responseObj.status = 1;
                        responseObj.data = results;
                        callback(responseObj);
                    } else {
                        responseObj.message = "No status updated"
                        responseObj.status = 0;
                        responseObj.data = [];
                        callback(responseObj);
                    }
                } else {
                    responseObj.message = error
                    responseObj.status = 0;
                    responseObj.data = [];
                    callback(responseObj);
                }
            });
        } else {
            responseObj.message = "PLease pass isActive field"
        }
    } else {
        responseObj.message = "PLease pass productId";
    }
};

getAlPurchasedProduct = function(req, resp) {

    /*
        Author : Vinay Jadhav
        Information ; This will give you all list of products thoseare buyed
     */
    var getProductListQuery = "SELECT prod.`created_at` ,prod.`modified_at` , prod.`name` , prod.`description`,prod." +
        "`seller_id`,prod.`quantity`,prod.`price`,prod.`barcode_color`, prod.`category_id` , prod.`sub_category`,  u.`id` as `userId` , u.`name` as userName , u.`email` as userEmail , u.`mobile` FROM \n" +
        "`tbl_products` prod  INNER JOIN `tbl_bought_products` tbp ON tbp.`product_id` = prod.`id`\n" +
        "INNER JOIN `tbl_users` u ON u.`id`= tbp.`buyer_id`";
    connection.query(getProductListQuery, function(error, results, fields) {
        var responseObj = {
            status: 0,
            message: "",
            data: []
        };
        if (!error) {
            if (results.length > 0) {
                responseObj.message = "Data fetched";
                responseObj.data = results
                responseObj, status = 1;
                resp.status(200).json(responseObj);
            } else {
                responseObj.message = "No data found";
                res.status(400).json(responseObj);
            }
        } else {
            responseObj.message = error;
            res.status(400).json(responseObj);
        }
    });

}

getPurchasedProductByUser = function(req, resp) {


    /*
        Author : Vinay Jadhav
        Information ; This will give you all list of products thoseare buyed by perticular buyer
     */

    var user_id = req.params.user_id;
    var getProductListQuery = "SELECT prod.`created_at` ,prod.`modified_at` , prod.`name` , prod.`description`,prod." +
        "`seller_id`,prod.`quantity`,prod.`price`,prod.`barcode_color`, prod.`category_id` , prod.`sub_category`,  u.`id` as `userId` , u.`name` as userName , u.`email` as userEmail , u.`mobile` FROM \n" +
        "`tbl_products` prod  INNER JOIN `tbl_bought_products` tbp ON tbp.`product_id` = prod.`id`\n" +
        "INNER JOIN `tbl_users` u ON u.`id`= tbp.`buyer_id` WHERE prod.`isActive`=1 AND u.`id`=" + user_id;

    connection.query(getProductListQuery, function(error, results, fields) {
        var responseObj = {
            status: 0,
            message: "",
            data: []
        };
        if (!error) {
            if (results.length > 0) {
                responseObj.message = "Data fetched";
                responseObj.data = results
                responseObj.status = 1;
                resp.status(200).json(responseObj);
            } else {
                responseObj.message = "No data found";
                res.status(400).json(responseObj);
            }
        } else {
            responseObj.message = error;
            res.status(400).json(responseObj);
        }
    });

}
module.exports = {

    create: create,
    getProductList: getProductList,
    getProductDetails: getProductDetails,
    getProductImages: getProductImages,
    getUsersProductsList: getUsersProductsList,
    doSearchProducts: doSearchProducts,
    getAllProductWithAllInformation: getAllProductWithAllInformation,
    deactivateProduct: deactivateProduct,
    updateProduct: updateProduct,
    getAlPurchasedProduct: getAlPurchasedProduct,
    getPurchasedProductByUser: getPurchasedProductByUser
        //getProductBarcode,getProductBarcode,
    }