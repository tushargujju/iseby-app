const catCtrl = require('./categories.ctrl');
const express = require('express');

const router = express.Router();


module.exports = function(app){

  router.get(
    '/',
    catCtrl.getAllCategories
  );

  router.post(
    '/',
    catCtrl.create
  );

app.use('/categories',router);
};
