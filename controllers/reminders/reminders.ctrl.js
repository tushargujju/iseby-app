const connection = require('../../config/database').connection;
var createReminder = function (req, res, next) {
    /*
                      Author : Vinay Jadhav,
                      Description : It will create a wishlist for a buyer
                      Return : Will return a wishlist id
                   */
    var responseObj = {
        status :  0 ,
        message : "",
        data : []
    };

    var reminderDateTime = req.body.date_time;
    var buyer_id = req.body.buyer_id;
    var isActive = req.body.isActive;
    var product_id = req.body.product_id;


    if(reminderDateTime!=null && reminderDateTime.length > 0){
        if(product_id!=null && product_id > 0){
            if (buyer_id!=null &&  buyer_id>0){
                if (isActive !=null && isActive in [0,1]){
                    var insertValues = [reminderDateTime ,  buyer_id ,product_id , isActive ];
                    var query = "INSERT INTO `tbl_reminder`(`datetime`, `buyer_id`, `product_id`, `isActive`)" +
                        "VALUES (?)";
                    connection.query(query, [insertValues] , function (error, results, fields) {
                        if (!error) {
                            if (results.affectedRows > 0) {
                                responseObj.status = 1,
                                    responseObj.message = "Data inserted",
                                    responseObj.data = results.insertId
                            } else {
                                responseObj.status = 0,
                                    responseObj.message = "Sorry data is not inserted",
                                    responseObj.data = []
                            }
                            res.status(200).json(responseObj);
                        } else {
                            responseObj.message = error;
                            res.status(401).json(responseObj);
                        }
                    });
                }else{
                    responseObj.message = "Please specify isActive from 0 & 1";
                    res.status(401).json(responseObj);
                }
            }else{
                responseObj.message = "Please specify buyer_id";
                res.status(401).json(responseObj);
            }
        }else{
            responseObj.message = "Please specify product_id";
            res.status(401).json(responseObj);
        }
    }else{
        responseObj.message = "Please specify date_time";
        res.status(401).json(responseObj);
    }

};
var getAllRemindersOfBuyer = function (req, res, next) {
    /*
           Author : Vinay Jadhav,
           Description : This will give you all wishlist information of perticular buyer
           Return : It will return an array of wishlist
        */
    var buyers_id = req.params.buyer_id;
    var query = "SELECT r.`reminder_id`, r.`datetime`, r.`buyer_id`, r.`product_id`, r.`isActive`, " +
                " p.`id` as `product_id`, p.`product_code` , p.`name` , p.`description` , p.`seller_id` , " +
                " p.`quantity`,p.`price`,p.`barcode_color`,p.`category_id`,p.`sub_category`,p.`created_at`, p.`modified_at` FROM " +
                " `tbl_reminder` r INNER JOIN `tbl_products` p ON p.`id` = r.`product_id` WHERE  r.`buyer_id`=? " +
                " AND r.`isActive`=?";

    connection.query(query,[buyers_id , 1] , function (error, results, fields) {
        var responseObj = {
            status: 0,
            message: "",
            data: []
        };
        if (!error) {
            if (results.length > 0) {
                    responseObj.status = 1,
                    responseObj.message = "Data found",
                    responseObj.data = results
            } else {
                    responseObj.status = 0;
                    responseObj.message = "No Records found",
                    responseObj.data = []
            }
            res.status(200).json(responseObj);
        } else {
            responseObj.message = error;
            res.status(401).json(responseObj);
        }
    });

}
var deleteReminders = function (req, res, next) {
                /*
                      Author : Vinay Jadhav,
                      Description : This will soft delete wishlist of buyer
                      Return : It will a status as 1 if deleted else 0
                   */
    var reminder_id = req.body.reminder_id;
    var responseObject = {
        status :  0 ,
        message : "",
        data : []
    };
    if(reminder_id!=null || reminder_id > 0){
        var query = "UPDATE `tbl_reminder` SET `isActive`= ? WHERE `reminder_id`=?";
        connection.query(query, [0 , reminder_id] , function (error, results, fields) {
            if (!error) {
                if (results.affectedRows > 0) {
                    responseObject.status = 1,
                        responseObject.message = "Data deletd",
                        responseObject.data = results
                } else {
                    responseObject.status = 0,
                        responseObject.message = results,
                        responseObject.data = []
                }
                res.status(200).json(responseObject);
            } else {
                responseObject.message = error
                res.status(401).json(responseObject);
            }
        });
    }else{
        responseObject.message = "wishlist_id  should not be null";
        res.status(401).json(responseObject);
    }
};

module.exports = {
    createReminder: createReminder,
    getAllRemindersOfBuyer  : getAllRemindersOfBuyer,
    deleteReminders: deleteReminders
};
