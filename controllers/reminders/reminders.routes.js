const reminders = require('./reminders.ctrl');
const express = require('express');

const router = express.Router();
module.exports = function(app){
  router.get(
    '/:buyer_id',
      reminders.getAllRemindersOfBuyer
  );
  router.post(
    '/',
      reminders.createReminder
  );
    router.delete(
        '/',
        reminders.deleteReminders
    );
    app.use('/reminders',router)
};
