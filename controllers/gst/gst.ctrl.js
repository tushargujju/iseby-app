const connection = require('../../config/database').connection;

function checkMendatoryFields(inputObject) {
    var allKeys = Object.keys(inputObject);
    var output = {
        isError: 0,
        message: ""
    };
    for (var counter = 0; counter < allKeys.length; counter++) {
        var currentKey = allKeys[counter];
        if (isNullOrUndefined(inputObject[currentKey])) {
            output.isError = 1;
            output.message = "Please specify ," + currentKey + ".It should not be null or undefined";
            break;
        } else if (inputObject[currentKey] == "") {
            output.isError = 1;
            output.message = "Please specify ," + currentKey + ".It should not be empty";
            break;
        }
    }
    return output;
}


var isNullOrUndefined = function(val) {
    return val === undefined || val == null ? 1 : 0;
}


var addgst = function(req, res, next) {
    /*
                      Author : Vinay Jadhav,
                      Description : This will add a GST
                      Return : Will return 1 or 0
                   */
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };

    var slabname = req.body.slabname;
    var sgst = req.body.sgst;
    var cgst = req.body.cgst;
    var igst = req.body.igst;
    var created_by = req.body.created_by;
    var modified_by = req.body.modified_by;
    var created_on = new Date();

    var checkCompulsoryFields = {slabname :  slabname , sgst :  sgst , cgst :  cgst , igst :  igst , created_by :  created_by , modified_by :  modified_by };
    var output = checkMendatoryFields(checkCompulsoryFields);
    if (output.isError == 0) {
        var query = "INSERT INTO `tblgst`( `slabname`, `sgst`, `cgst`, `igst`, `created_by`, `modified_by`, `created_on`) VALUES   (?)";
        var insertValues = [slabname, sgst, cgst, igst, created_by, modified_by, created_on ];
        connection.query(query, [insertValues], function(error, results, fields) {
            if (!error) {
                if (results.affectedRows > 0) {
                    responseObj.status = 1,
                        responseObj.message = "Data inserted",
                        responseObj.data = results 
                } else {
                    responseObj.status = 0,
                        responseObj.message = "Sorry data is not inserted",
                        responseObj.data = []
                }
                res.status(200).json(responseObj);
            } else {
                responseObj.message = error;
                res.status(401).json(responseObj);
            }
        });
    } else {
        responseObj.message = output.message;
        res.status(400).json(responseObj);
    }
}

var updateGst = function(req, res, next) {
    /*
                      Author : Vinay Jadhav,
                      Description : This will update a GST
                      Return : Will return 1 or 0
                   */
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };

    var slabname = req.body.slabname;
    var sgst = req.body.sgst;
    var cgst = req.body.cgst;
    var igst = req.body.igst;
    var modified_by = req.body.modified_by;
    var id = req.body.id;

    var checkCompulsoryFields = {slabname :  slabname , sgst :  sgst , cgst :  cgst , igst :  igst ,  modified_by :  modified_by , id };
    var output = checkMendatoryFields(checkCompulsoryFields);
    if (output.isError == 0) {
        
        var query = "UPDATE `tblgst` SET `slabname`= '"+slabname+"' ,`sgst`="+sgst+" ,`cgst`= "+cgst+" ,`igst`="+igst+" , `modified_by`="+modified_by+"  WHERE `id`= "+id;

        var updateValues = [slabname, sgst, cgst, igst,   modified_by,   id];
        connection.query(query,  function(error, results, fields) {
            if (!error) {
                if (results.affectedRows > 0) {
                    responseObj.status = 1,
                        responseObj.message = "Data updated",
                        responseObj.data = results 
                } else {
                    responseObj.status = 0,
                        responseObj.message = "Sorry data is not updated",
                        responseObj.data = []
                }
                res.status(200).json(responseObj);
            } else {
                responseObj.message = error;
                res.status(401).json(responseObj);
            }
        });
    } else {
        responseObj.message = output.message;
        res.status(400).json(responseObj);
    }
}


var deleteGst = function(req, res, next) {
    /*
                      Author : Vinay Jadhav,
                      Description : This will delete a GST
                      Return : Will return 1 or 0
                   */
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };

    var gstId = req.body.gstId;
    

    var checkCompulsoryFields = {gstId :  gstId   };
    var output = checkMendatoryFields(checkCompulsoryFields);
    if (output.isError == 0) {
        var query = "DELETE FROM  `tblgst` WHERE id=?";
        var gstId = [gstId];
        connection.query(query, [gstId], function(error, results, fields) {
            if (!error) {
                if (results.affectedRows > 0) {
                        responseObj.status = 1,
                        responseObj.message = "Data deleted",
                        responseObj.data = results 
                } else {
                    responseObj.status = 0,
                        responseObj.message = "Sorry data is not deleted",
                        responseObj.data = []
                }
                res.status(200).json(responseObj);
            } else {
                responseObj.message = error;
                res.status(400).json(responseObj);
            }
        });
    } else {
        responseObj.message = output.message;
        res.status(400).json(responseObj);
    }
}
 
module.exports = {
    addgst: addgst,
    updateGst : updateGst,
    deleteGst : deleteGst
};