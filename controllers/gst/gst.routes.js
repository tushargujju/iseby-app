const gst = require('./gst.ctrl');
const express = require('express');

const router = express.Router();

module.exports = function(app){
  router.post(
    '/',
      gst.addgst
  );

  router.put(
    '/',
      gst.updateGst
  );

   router.delete(
    '/',
      gst.deleteGst
  );

  
    app.use('/gst',router);
};
