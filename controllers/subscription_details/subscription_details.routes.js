const subscription = require('./subscription_details.ctrl');
const express = require('express');

const router = express.Router();


module.exports = function(app){

  router.get(
    '/getAllSubscriptionsDetails',
      subscription.getAllSubscriptionsDetails
  );

    router.get(
        '/getSubscriptionDetailsById/:subscription_details_id',
        subscription.getSubscriptionDetailsById
    );
  router.post(
    '/createSubscriptionDetails',
      subscription.createSubscriptionDetails
  );

    router.delete(
        '/deleteSubscriptionDetails/:subscription_details_id',
        subscription.deleteSubscriptionDetails
    );

    router.put(
        '/updateSubscriptionDetailsById',
        subscription.updateSubscriptionDetailsById
    );

    app.use('/subscription_details',router);
};
