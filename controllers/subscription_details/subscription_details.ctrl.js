const connection = require('../../config/database').connection;

var getAllSubscriptionsDetails = function (req, res, next) {
    var query = "SELECT * FROM `tbl_subscription_details`";
    connection.query(query, function (error, results, fields) {
        /*
            Author : Vinay Jadhav,
            Description : This will have a all subscription details which are currenlty active by seller
            Return : It will return an array of objects
         */
        var responseObj = {
            status: 0,
            message: "",
            data: []
        };
        if (!error) {
            if (results.length > 0) {
                responseObj.status = 1,
                    responseObj.message = "Data found"
                responseObj.data = results
            } else {
                responseObj.status = 0;
                responseObj.message = "No Records found"
                responseObj.data = []
            }
            res.status(200).json(responseObj);
        } else {
            res.status(401).json(responseObj);
        }
    });

}

var getSubscriptionDetailsById = function (req, res, next) {

    /*
       Author : Vinay Jadhav,
       Description : This will have a perticular subscription details  which are currenlty active by seller
       Return : It will return an array of objects
    */
    var subscription_details_id = req.params.subscription_details_id;
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };
    if (subscription_details_id != null && subscription_details_id > 0) {
        var query = "SELECT * FROM tbl_subscription_details where `seller_id` = '" + subscription_details_id+  "'";
        connection.query(query, function (error, results, fields) {
            if (!error) {
                if (results.length > 0) {
                    responseObj.status = 1,
                        responseObj.message = "Data found"
                    responseObj.data = results
                } else {
                    responseObj.status = 0;
                    responseObj.message = "No Records found"
                    responseObj.data = []
                }
                res.status(200).json(responseObj);
            } else {
                res.status(401).json(responseObj);
            }
        });
    } else {
        responseObj.message = "subscription_id should not be a null or 0"
        res.status(401).json(responseObj);
    }
};

var updateSubscriptionDetailsById = function (req, res, next) {

    var responseObj = {
        status: 0,
        message: "",
        data: []
    };
    var subscription_id = req.body.subscription_id;
    var seller_id = req.body.seller_id;
    var created_date = req.body.created_date;
    var expiry_date = req.body.expiry_date;
    var remaining_days = req.body.remaining_days;
    var subscription_details_id = req.body.subscription_details_id;

    if (subscription_details_id != null && subscription_details_id > 0)
        if (subscription_id != null && subscription_id > 0) {
            if (seller_id != null && seller_id > 0) {
                if (created_date != null && created_date.length > 0) {
                    if (expiry_date != null && expiry_date.length > 0) {
                        var query = "UPDATE `tbl_subscription_details` SET " +
                            "`subscription_id`= " + subscription_id + " ,`seller_id`= " + seller_id + "," +
                            "`created_date`='" + created_date + "' , `expiry_date`= '" + expiry_date + "'," +
                            "remaining_days = " + remaining_days + " WHERE `subscription_details_id` =" + subscription_details_id;
                        connection.query(query, function (error, results, fields) {
                            if (!error) {
                                if (results.affectedRows > 0) {
                                    responseObj.status = 1,
                                        responseObj.message = "Data updated",
                                        responseObj.data = results
                                } else {
                                    responseObj.status = 0,
                                        responseObj.message = error,
                                        responseObj.data = []
                                }
                                res.status(200).json(responseObj);
                            } else {
                                responseObj.message = error
                                res.status(401).json(responseObj);
                            }
                        });
                    } else {
                        responseObj.message = "Please specify plan expiry_date";
                        res.status(401).json(responseObj);
                    }
                } else {
                    responseObj.message = "Please specify plan created_date";
                    res.status(401).json(responseObj);
                }

            } else {
                responseObj.message = "Please specify subscription_id";
                res.status(401).json(responseObj);
            }
        } else {
            responseObj.message = "Please subscription_details_id";
            res.status(401).json(responseObj);
        }

};

var createSubscriptionDetails = function (req, res, next) {
    /*
                      Author : Vinay Jadhav,
                      Description : This will create a new subscription details
                      Return : It will return an array of objects
                   */
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };

    var subscription_id = req.body.subscription_id;
    var seller_id = req.body.seller_id;
    var created_date = req.body.created_date;
    var expiry_date = req.body.expiry_date;
    var remaining_days = req.body.remaining_days;

    var insertValues = [subscription_id, seller_id, created_date, expiry_date, remaining_days];
    if (subscription_id != null && subscription_id > 0) {
        if (seller_id != null && seller_id > 0) {
            if (created_date != null || created_date.length > 0) {
                if (expiry_date != null && expiry_date.length > 0) {
                    if (remaining_days != null && remaining_days > 0) {
                        var query = "INSERT INTO `tbl_subscription_details`(`subscription_id`, " +
                            "`seller_id`, `created_date`, `expiry_date`, `remaining_days`) values (?)";

                        connection.query(query, [insertValues], function (error, results, fields) {
                            if (!error) {
                                if (results.affectedRows > 0) {
                                    responseObj.status = 1,
                                        responseObj.message = "Data inserted",
                                        responseObj.data = results.insertId
                                } else {
                                    responseObj.status = 0,
                                        responseObj.message = "Sorry data is not inserted",
                                        responseObj.data = []
                                }
                                res.status(200).json(responseObj);
                            } else {
                                responseObj.message = error;
                                res.status(401).json(responseObj);
                            }
                        });
                    } else {
                        responseObj.message = "Please specify plan remaining_days";
                        res.status(401).json(responseObj);
                    }
                } else {
                    responseObj.message = "Please specify plan expiry_date";
                    res.status(401).json(responseObj);
                }
            } else {
                responseObj.message = "Please specify plan created_date"
                res.status(401).json(responseObj);
            }
        } else {
            responseObj.message = "Please specify plan seller_id"
            res.status(401).json(responseObj);
        }
    } else {
        responseObj.message = "Please specify subscription_id"
        res.status(401).json(responseObj);
    }
}

var deleteSubscriptionDetails = function (req, res, next) {
    /*
          Author : Vinay Jadhav,
          Description : This will delete subscription plan
          Return : It will a status as 1 if deleted else 0
       */
    var subscription_id = req.params.subscription_details_id;
    var responseObject = {
        status: 0,
        message: "",
        data: []
    };

    if (subscription_id != null || subscription_id >= 0) {
        var query = "DELETE FROM `tbl_subscription_details` WHERE `subscription_details_id`=?";
        connection.query(query, [subscription_id], function (error, results, fields) {
            if (!error) {

                if (results.affectedRows > 0) {
                    responseObject.status = 1,
                        responseObject.message = "Data deletd",
                        responseObject.data = results
                } else {
                    responseObject.status = 0,
                        responseObject.message = results,
                        responseObject.data = []
                }
                res.status(200).json(responseObject);
            } else {
                responseObject.message = error
                res.status(401).json(responseObject);
            }
        });
    } else {
        responseObject.message = "subscription_details_id should be grater than 0";
        res.status(401).json(responseObject);
    }
}

var getSubscriptionDetailsBySellerId = function (inputData, callback) {

    /*
       Author : Vinay Jadhav,
       Description : This will have a perticular subscription details  which are currenlty active by seller
       Return : It will return an array of objects
    */
    var seller_id = inputData.seller_id;
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };
    if (seller_id != null && seller_id > 0) {
        var query = "SELECT * FROM tbl_subscription_details where `seller_id` = ?";
        connection.query(query, [subscription_details_id], function (error, results, fields) {
            if (!error) {
                if (results.length > 0) {
                    responseObj.status = 1,
                        responseObj.message = "Data found",
                        responseObj.data = results
                } else {
                    responseObj.status = 0;
                    responseObj.message = "No Records found",
                        responseObj.data = []
                }
                callback(responseObj)
            } else {
                responseObj.message = error;
                callback(responseObj);
            }
        });
    } else {
        responseObj.message = "seller_id should not be a null or 0"
        callback(responseObj);
    }
};


var getAllSubscriptionDetailsWithUserInformation = function (callback) {

    /*
       Author : Vinay Jadhav,
       Description : This will give you all subscription with seller information and subscription information
       Return : It will return an array of objects
    */
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };
    var query = "SELECT tsd.`subscription_id` , tsd.`created_date` , tsd.`expiry_date` , " +
        " u.`id` as `userId` , u.`name` as userName , u.`email` as userEmail , u.`mobile`," +
        " ts.`subscription_id`,ts.`plan` , ts.`description` , ts.`ammount`" +
        "FROM" +
        "`tbl_subscription_details` tsd INNER JOIN `tbl_users` u ON tsd.`seller_id` = u.`id`" +
        " INNER JOIN `tbl_subscription` ts ON ts.`subscription_id` = tsd.`subscription_id`";

    connection.query(query, function (error, results, fields) {
        if (!error) {
            if (results.length > 0) {
                responseObj.status = 1;
                    responseObj.message = "Data found";
                    responseObj.data = results
            } else {
                responseObj.status = 0;
                responseObj.message = "No Records found";
                    responseObj.data = []
            }
            callback(responseObj)
        } else {
            responseObj.message = error;
            callback(responseObj);
        }
    });
};

module.exports = {
    getAllSubscriptionsDetails: getAllSubscriptionsDetails,
    getSubscriptionDetailsById: getSubscriptionDetailsById,
    updateSubscriptionDetailsById: updateSubscriptionDetailsById,
    createSubscriptionDetails: createSubscriptionDetails,
    deleteSubscriptionDetails: deleteSubscriptionDetails,
    getSubscriptionDetailsBySellerId: getSubscriptionDetailsBySellerId,
    getAllSubscriptionDetailsWithUserInformation: getAllSubscriptionDetailsWithUserInformation
};
