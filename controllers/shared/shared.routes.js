const sharedCtrl = require('./shared.ctrl');
const express = require('express');

const router = express.Router();


module.exports = function(app){

  router.get(
    '/:user_id',
    sharedCtrl.getAllSharedProductsByUser
  );

  router.get(
    '/withme/:user_id',
    sharedCtrl.getAllSharedProductsWithUser
  );

  router.post(
    '/',
    sharedCtrl.share
  );

app.use('/shared',router);
};
