const connection = require('../../config/database').connection;
const subscription = require("../subscription/subscription.ctrl");

var getAllSharedProductsByUser = function (req, res, next) {
    /*
        Author : Vinay Jadhav
        Information : THis API will get all orders of user if he subscribed or his subscription is not over
                      Here I'm using seller_id as userId coming as an input
     */
    var responseObj = {};
    var user_id = req.params.user_id;
    subscription.checkSubscriptionOfUser({sellerId: user_id}, function (response) {
        if (response.status) {
            if (response.data.isExpired == 0) {
                /*var getproductsQuery = "SELECT shared.product_id, shared.shared_by, seller_info.`shop_name`, seller_info.`shop_address`, user.`name` as `seller_name` , prod.* FROM tbl_shared_products shared " + "JOIN tbl_products prod ON shared.product_id= prod.id INNER JOIN `tbl_subscription_details` tsd " + "ON tsd.`seller_id` = prod.`seller_id` INNER JOIN `tbl_seller_info` seller_info ON seller_info.`id` = prod.`seller_id` INNER JOIN `tbl_users` user ON user.`id` = seller_info.`user_id` WHERE shared.shared_by = '" + user_id + "'" + "GROUP BY shared.product_id";*/
				
				var getproductsQuery = "SELECT `tbl_shared_products`.`shared_by`, `tbl_products`.`id`, `tbl_products`.`seller_id`, `tbl_seller_info`.`shop_name`, `tbl_products`.`product_code`, `tbl_products`.`name`, `tbl_products`.`description`, `tbl_products`.`seller_id`, `tbl_products`.`quantity`, `tbl_products`.`price`, `tbl_products`.`discount_per`, `tbl_products`.`barcode_color` FROM `tbl_shared_products`, `tbl_products`, `tbl_seller_info` WHERE `tbl_shared_products`.`shared_by` = '" + user_id + "' AND `tbl_products`.`id` = `tbl_shared_products`.`product_id` AND `tbl_products`.`seller_id` = `tbl_seller_info`.`user_id` AND `tbl_products`.`isActive` = 1";
					
					
				//var getproductsQuery = "SELECT * FROM tbl_shared_products WHERE shared_by='"+user_id+"'";
					
					
                connection.query(getproductsQuery, function (error, results, fields) {
                    //if(error)throw error;
                    responseObj['status'] = 1;
                    responseObj['message'] = 'Shared Products fetched successfully';
                    responseObj['data'] = results;

                    res.status(200).json(responseObj);
                });
            } else {
                responseObj['status'] = 0;
                responseObj['product_id'] = 0;
                responseObj['message'] = response.message;
                res.status(200).json(responseObj);
            }
        } else {
            responseObj['status'] = 0;
            responseObj['product_id'] = 0;
            responseObj['message'] = response.message;
            res.status(401).json(responseObj);
        }
    });
    //Get Shared Products By user

}

var getAllSharedProductsWithUser = function (req, res, next) {
    var user_id = req.params.user_id;
    var responseObj = {};
    subscription.checkSubscriptionOfUser({sellerId: user_id}, function (response) {
        if (response.status) {
            if (response.data.isExpired == 0) {
                //Get Shared Products By user
                var getproductsQuery = "SELECT shared.product_id, shared.shared_with, prod.* FROM tbl_shared_products shared JOIN tbl_products prod ON shared.product_id= prod.id WHERE shared.shared_with = '" + user_id + "' GROUP BY shared.product_id";
                connection.query(getproductsQuery, function (error, results, fields) {
                    responseObj['status'] = 1;
                    responseObj['message'] = 'Shared Products fetched successfully';
                    responseObj['data'] = results;
                    res.status(200).json(responseObj);
                });
            } else {
                responseObj['status'] = 0;
                responseObj['product_id'] = 0;
                responseObj['message'] = response.message;
                res.status(200).json(responseObj);
            }
        } else {
            responseObj['status'] = 0;
            responseObj['product_id'] = 0;
            responseObj['message'] = response.message;
            res.status(401).json(responseObj);
        }
    });


}

var share = function (req, res, next) {
    var xBody = req.body;

    var shared_by = xBody.shared_by;
    var shared_with = 0;

    var product_id = xBody.product_id;
    var share_type = xBody.share_type;

    if (share_type == 2) {
        shared_with = xBody.shared_with;
    }

    var created_at = Math.round((new Date()).getTime() / 1000);
    var modified_at = Math.round((new Date()).getTime() / 1000);

    var responseObj = {};
    subscription.checkSubscriptionOfUser({sellerId: shared_by}, function (response) {
        if (response.status) {
            if (response.data.isExpired == 0) {
                var checkIfSharedQuery = "";
                if (share_type == 1) {
                    checkIfSharedQuery = "SELECT COUNT(*) AS COUNT FROM tbl_shared_products WHERE shared_by='" + shared_by + "' AND product_id='" + product_id + "' AND share_type='" + share_type + "'";
                } else {
                    checkIfSharedQuery = "SELECT COUNT(*) AS COUNT FROM tbl_shared_products WHERE shared_by='" + shared_by + "' AND shared_with='" + shared_with + "' AND share_type='" + share_type + "' AND product_id='" + product_id + "'";
                }
                connection.query(checkIfSharedQuery, function (error, results, fields) {

                    if (error) throw error;
                    console.log(results[0]['COUNT']);

                    if (results[0]['COUNT'] == 0) {
                        var query_add_cat = "";
                        if (share_type == 2) {
                            query_share_prod = "INSERT INTO tbl_shared_products (shared_by, shared_with, product_id, share_type, created_at, modified_at) VALUES ('" + shared_by + "', '" + shared_with + "', '" + product_id + "', '" + share_type + "', '" + created_at + "', '" + modified_at + "')";
                        } else {
                            query_share_prod = "INSERT INTO tbl_shared_products (shared_by, product_id, share_type, created_at, modified_at) VALUES ('" + shared_by + "',  '" + product_id + "', '" + share_type + "', '" + created_at + "', '" + modified_at + "')";
                        }

                        connection.query(query_share_prod, function (error, results, fields) {
                            if (error) throw error;

                            //Add Point Earned
                            connection.query("SELECT * FROM tbl_users WHERE id = '" + shared_by + "'", function (error, results, fields) {
                                if (error) throw error;
                                console.log(results);
                                var userPoints = parseInt(results[0]['points_earned']);
                                userPoints = userPoints + 1;
                                console.log(results);
                                var query_update_points = "UPDATE tbl_users SET points_earned = '" + userPoints + "' WHERE id='" + shared_by + "'";
                                connection.query(query_update_points, function (error, results, fields) {
                                    if (error) throw error;
                                    responseObj['status'] = 1;
                                    responseObj['message'] = "Product shared successfully.";
                                    res.status(200).json(responseObj);
                                });
                            });
                        });

                    } else {
                        responseObj['status'] = 0;
                        responseObj['message'] = "Product already shared.";
                        res.status(400).json(responseObj);
                    }
                });
            } else {
                responseObj['status'] = 0;
                responseObj['product_id'] = 0;
                responseObj['message'] = response.message;
                res.status(200).json(responseObj);
            }
        } else {
            responseObj['status'] = 0;
            responseObj['product_id'] = 0;
            responseObj['message'] = response.message;
            res.status(401).json(responseObj);
        }
    });
}


module.exports = {

    getAllSharedProductsByUser: getAllSharedProductsByUser,
    getAllSharedProductsWithUser: getAllSharedProductsWithUser,
    share: share,

};
