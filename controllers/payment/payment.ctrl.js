const connection = require('../../config/database').connection;

function checkMendatoryFields(inputObject) {
    var allKeys = Object.keys(inputObject);
    var output = {
        isError: 0,
        message: ""
    };
    for (var counter = 0; counter < allKeys.length; counter++) {
        var currentKey = allKeys[counter];
        if (isNullOrUndefined(inputObject[currentKey])) {
            output.isError = 1;
            output.message = "Please specify ," + currentKey + ".It should not be null or undefined";
            break;
        } else if (inputObject[currentKey] == "") {
            output.isError = 1;
            output.message = "Please specify ," + currentKey + ".It should not be empty";
            break;
        }
    }
    return output;
}


var isNullOrUndefined = function(val) {
    return val === undefined || val == null ? 1 : 0;
}


var addTransaction = function(req, res, next) {
    /*
                      Author : Vinay Jadhav,
                      Description : This will add transaction
                      Return : Will return 1 or 0
                   */
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };

    var Product_id = req.body.Product_id;
    var Seller_id = req.body.Seller_id;
    var Buyer_id = req.body.Buyer_id;
    var Trans_Amount = req.body.Trans_Amount;
    var Trans_date = req.body.Trans_date;
    var Status = req.body.Status;
    var Created_by = req.body.Created_by;
    var Modified_by = req.body.Modified_by;
    var created_on = new Date();
    var trans_id = req.body.trans_id;

    var checkCompulsoryFields = {Product_id :  Product_id , Seller_id :  Seller_id , Buyer_id :  Buyer_id , Trans_Amount :  Trans_Amount , Trans_date :  Trans_date , Status :  Status , Created_by :  Created_by , Modified_by :  Modified_by , created_on :  created_on , trans_id  : trans_id };
    var output = checkMendatoryFields(checkCompulsoryFields);
    if (output.isError == 0) {
        var query = "INSERT INTO `pg_trans`(  `Product_id`, `Seller_id`, `Buyer_id`, `Trans_Amount`, `Trans_date`, `Status`, `Created_by`, `Modified_by`, `Created_on`, `trans_id`) VALUES (?)";
        var insertValues = [Product_id, Seller_id, Buyer_id, Trans_Amount, Trans_date, Status, Created_by, Modified_by, created_on, trans_id ];
        connection.query(query, [insertValues], function(error, results, fields) {
            if (!error) {
                if (results.affectedRows > 0) {
                    responseObj.status = 1,
                        responseObj.message = "Data inserted",
                        responseObj.data = results 
                } else {
                    responseObj.status = 0,
                        responseObj.message = "Sorry data is not inserted",
                        responseObj.data = []
                }
                res.status(200).json(responseObj);
            } else {
                responseObj.message = error;
                res.status(401).json(responseObj);
            }
        });
    } else {
        responseObj.message = output.message;
        res.status(400).json(responseObj);
    }
}

var getAllTransactionByUserId = function (req, res , next){
      /*
                      Author : Vinay Jadhav,
                      Description : This will get all transaction of buyer
                      Return : Will return all buyer transaction information
                   */
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };

    var userId = req.params.userId;

    if(userId!=null && userId>0){

        var query = "SELECT * FROM `pg_trans` WHERE  `Buyer_id` = ? "
        var insertValues = [userId]
        connection.query(query, [insertValues], function(error, results, fields) {
            if (!error) {
                if (results.length > 0) {
                        responseObj.status = 1,
                        responseObj.message = "Data found",
                        responseObj.data = results 
                } else {
                    responseObj.status = 0,
                        responseObj.message = "Sorry data is not found",
                        responseObj.data = []
                }
                res.status(200).json(responseObj);
            } else {
                responseObj.message = error;
                res.status(401).json(responseObj);
            }
        });
    }else{
        responseObj.message = "User id should not be null or 0";
        res.status(400).json(responseObj);
    }

}
module.exports = {
    addTransaction: addTransaction,
    getAllTransactionByUserId : getAllTransactionByUserId
    
};