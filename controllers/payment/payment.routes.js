const payment = require('./payment.ctrl');
const express = require('express');

const router = express.Router();

module.exports = function(app){
  router.post(
    '/',
      payment.addTransaction
  );

  router.get(
    '/:userId',
      payment.getAllTransactionByUserId
  );
    
    app.use('/payment',router);
};
