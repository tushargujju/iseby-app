const subscription = require('./subscription.ctrl');
const express = require('express');

const router = express.Router();


module.exports = function(app){

  router.get(
    '/getAllSubscriptions',
      subscription.getAllSubscriptions
  );

    router.get(
        '/getSubscriptionById/:subscription_id',
        subscription.getSubscriptionById
    );
  router.post(
    '/create',
      subscription.createSubscription
  );

    router.delete(
        '/deleteSubscription/:subscription_id',
        subscription.deleteSubscription
    );

    router.put(
        '/',
        subscription.updateSubscriptionById
    );

    router.post(
        '/check',
        subscription.subscriptionExpiryCheckAPI
    );

    app.use('/subscription',router);
};
