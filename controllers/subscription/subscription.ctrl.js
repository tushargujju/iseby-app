var moment = require('moment');
const connection = require('../../config/database').connection;

var getAllSubscriptions = function (req, res, next) {
    var query = "SELECT * FROM tbl_subscription";
    connection.query(query, function (error, results, fields) {
        /*
            Author : Vinay Jadhav,
            Description : This will have a all subscription which are currenlty active by seller
            Return : It will return an array of objects
         */
        var responseObj = {
            status: 0,
            message: "",
            data: []
        };
        if (!error) {
            if (results.length > 0) {
                    responseObj.status = 1,
                    responseObj.message = "Data found"
                    responseObj.data = results
            } else {
                    responseObj.status = 0;
                    responseObj.message = "No Records found"
                    responseObj.data = []
            }
            res.status(200).json(responseObj);
        } else {
            res.status(401).json(responseObj);
        }
    });

}

var getSubscriptionById = function (req , res , next) {

            /*
               Author : Vinay Jadhav,
               Description : This will have a perticular subscription which are currenlty active by seller
               Return : It will return an array of objects
            */
    var subscription_id = req.params.subscription_id;
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };
    if(subscription_id!=null || subscription_id>0){
        var query = "SELECT * FROM tbl_subscription where `subscription_id` = ?";
        connection.query(query, [subscription_id] , function (error, results, fields) {
            if (!error) {
                if (results.length > 0) {
                    responseObj.status = 1,
                        responseObj.message = "Data found"
                    responseObj.data = results
                } else {
                    responseObj.status = 0;
                    responseObj.message = "No Records found"
                    responseObj.data = []
                }
                res.status(200).json(responseObj);
            } else {
                res.status(401).json(responseObj);
            }
        });
    }else{
        responseObj.message = "subscription_id should not be a null or 0"
        res.status(401).json(responseObj);
    }
};

var updateSubscriptionById = function (req , res , next) {

    var responseObj = {
        status: 0,
        message: "",
        data: []
    };
    var plan = req.body.plan;
    var description = req.body.description;
    var amount = req.body.ammount;
    var subscription_id = req.body.subscription_id;

    if(subscription_id!=null || subscription_id >0){
        if(plan!=null || plan != ""){
            if (description!=null || description!=""){
                if (amount!=null || amount>0){
                    var insertValues = [plan , description , amount , subscription_id];
                    var query = "UPDATE `tbl_subscription` SET " +
                        "`plan`= '"+plan+"' ,`description`= '"+description+"'," +
                        "`ammount`='"+amount+"' WHERE `subscription_id` ="+subscription_id;

                    connection.query(query , function (error, results, fields) {
                        if (!error) {
                            if (results.affectedRows > 0) {
                                responseObj.status = 1,
                                    responseObj.message = "Data updated",
                                    responseObj.data = results
                            } else {
                                responseObj.status = 0,
                                    responseObj.message = error,
                                    responseObj.data = []
                            }
                            res.status(200).json(responseObj);
                        } else {
                            responseObj.message = error
                            res.status(401).json(responseObj);
                        }
                    });
                }else{
                    responseObj.message = "Please specify plan amount";
                    res.status(401).json(responseObj);
                }
            }else{
                responseObj.message = "Please specify plan description";
                res.status(401).json(responseObj);
            }
        }else{
            responseObj.message = "Please specify plan";
            res.status(401).json(responseObj);
        }
    }else{
        responseObj.message = "Please subscription_id";
        res.status(401).json(responseObj);
    }

};

var createSubscription = function (req, res, next) {
    /*
                      Author : Vinay Jadhav,
                      Description : This will create a new subscription plan
                      Return : It will return an array of objects
                   */
    var responseObj = {
        status :  0 ,
        message : "",
        data : []
    };

    var plan = req.body.plan;
    var description = req.body.description;
    var amount = req.body.amount;
    var insertValues = [plan , description , amount];
    if(plan!=null || plan != ""){
        if (description!=null || description!=""){
            if (amount!=null || amount>0){
                var query = "INSERT INTO `tbl_subscription`(`plan`, `description`, `ammount`) " +
                    "VALUES (?)";

                connection.query(query, [insertValues] , function (error, results, fields) {
                    if (!error) {
                        if (results.affectedRows > 0) {
                                responseObj.status = 1,
                                responseObj.message = "Data inserted",
                                responseObj.data = results.insertId
                        } else {
                                responseObj.status = 0,
                                responseObj.message = "Sorry data is not inserted",
                                responseObj.data = []
                        }
                        res.status(200).json(responseObj);
                    } else {
                        responseObj.message = error;
                        res.status(401).json(responseObj);
                    }
                });
            }else{
                responseObj.message = "Please specify plan amount"
                res.status(401).json(responseObj);
            }
        }else{
            responseObj.message = "Please specify plan description"
            res.status(401).json(responseObj);
        }
    }else{
        responseObj.message = "Please specify plan"
        res.status(401).json(responseObj);
    }
}

var deleteSubscription = function (req, res, next) {
                /*
                      Author : Vinay Jadhav,
                      Description : This will delete subscription plan
                      Return : It will a status as 1 if deleted else 0
                   */
    var subscription_id = req.params.subscription_id;
    console.warn("Warning in delete subscription controller");
    var responseObject = {
        status :  0 ,
        message : "",
        data : []
    };

    if(subscription_id!=null || subscription_id>=0){
        var query = "DELETE FROM `tbl_subscription` WHERE `subscription_id`=?";
        connection.query(query, [subscription_id] , function (error, results, fields) {
            if (!error) {

                if (results.affectedRows > 0) {
                    responseObject.status = 1,
                        responseObject.message = "Data deletd",
                        responseObject.data = results
                } else {
                    responseObject.status = 0,
                        responseObject.message = results,
                        responseObject.data = []
                }
                res.status(200).json(responseObject);
            } else {
                responseObject.message = error
                res.status(401).json(responseObject);
            }
        });
    }else{
        responseObject.message = "Subscription id should not be null";
        responseObject.message = "subscription_id should be grater than 0";
        res.status(401).json(responseObject);
    }
}


var subscriptionExpiryCheckAPI  = function (req, resp){
    var inputData = {sellerId : req.body.sellerId};

    checkSubscriptionOfUser(inputData , function (response) {
        resp.status(200).json(response);
    });
}

var checkSubscriptionOfUser  = function (inputData, callback) {
    /*
            Author : Vinay Jadhav,
            Description : This method will check whether user subscription is expired or not.
            Return : It will return an object with 0 o 1 value.
                     If subscription is not expired then will return 1 else return 0
         */
    var userId = inputData.sellerId;
    var responseObj = {
        status: 0,
        message: "",
        data: {
            subscriptionExpiryDate : "",
            isExpired : 1
        }
    };
    if(userId!=null || userId  > 0){
        var query = "SELECT * FROM tbl_subscription_details where `seller_id` = ?";
        connection.query(query, [userId] , function (error, results, fields) {
            if (!error) {
                if (results.length > 0) {
                        var todaysDate = new Date();
                        var subscriptionInfo = results[0];
                        var expiryDate = moment(subscriptionInfo.expiry_date, 'YYYY-MM-DD HH:MM:SS');
                        var todaysDate = moment(todaysDate , 'YYYY-MM-DD HH:MM:SS');
                        var remainingDays = expiryDate.diff(todaysDate, 'days');

                        if(remainingDays > 0){
                                responseObj.status = 1,
                                responseObj.message = "Subscription is not expired",
                                responseObj.data.subscriptionExpiryDate = expiryDate,
                                responseObj.data.isExpired= 0
                        }else{
                                responseObj.status = 1,
                                responseObj.message = "Subscription is expired",
                                responseObj.data.subscriptionExpiryDate = expiryDate,
                                responseObj.data.isExpired= 1
                        }
                } else {
                    responseObj.status = 0;
                    responseObj.message = "No subscriptions records found for this seller"
                }
                callback(responseObj);
            } else {
                callback(responseObj);
            }
        });
    }else{
        responseObj.message = "subscription_id should not be a null or 0"
        callback(responseObj);
    }
}


module.exports = {
    getAllSubscriptions : getAllSubscriptions,
    createSubscription : createSubscription,
    deleteSubscription : deleteSubscription,
    updateSubscriptionById :updateSubscriptionById,
    getSubscriptionById : getSubscriptionById,
    checkSubscriptionOfUser : checkSubscriptionOfUser,
    subscriptionExpiryCheckAPI: subscriptionExpiryCheckAPI
};
