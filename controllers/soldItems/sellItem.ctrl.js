const connection = require('../../config/database').connection;
var sellItem = function (inputData , callback) {
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };
    var buyer_id   =  inputData.buyer_id;
    var seller_id =inputData.seller_id;
    var productId = inputData.productId;
    var sold_on = new Date();
    if(buyer_id!=null && buyer_id>0){
        if(seller_id!=null && seller_id>0){
            if(productId !=null && productId >0){
              var insertValues= [buyer_id , seller_id , productId , sold_on];
                var query = "INSERT INTO `tbl_solditems`(`buyer_id`, `seller_id`, `productId`, `sold_on`) VALUES(?) ";
                connection.query(query, [insertValues] , function (error, results, fields) {
                    if (!error) {
                        if (results.affectedRows > 0) {
                            responseObj.status = 1,
                                responseObj.message = "It is sold"
                            responseObj.data = results
                        } else {
                            responseObj.status = 0;
                            responseObj.message = "No Records found"
                            responseObj.data = []
                        }
                        callback(responseObj);
                    } else {
                        responseObj.message = error;
                        callback(responseObj);
                    }
                });
            }else{
                responseObj.message = "productId should not be null";
                callback(responseObj);            }
        }else{
            responseObj.message = "seller_id should not be null";
            callback(responseObj);        }
    }else{
        responseObj.message = "buyer_id should not be null";
        callback(responseObj);
    }
};

var sellTheProduct = function(req, resp){
    var buyer_id   = req.body.buyer_id;
    var seller_id = req.body.seller_id;
    var productId = req.body.productId;
    var inputData = {buyer_id: buyer_id , seller_id : seller_id , productId : productId};
    sellItem(inputData , function (response) {
        if(response.status){
            resp.status(200).json(response);
        }else{
            resp.status(200).json(response);
        }
    });
};


module.exports =
    {
  sellItem:sellTheProduct,
        sellItemFunction : sellItem,
};
