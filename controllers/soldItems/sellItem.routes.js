const soldItem = require('./sellItem.ctrl');
const express = require('express');

const router = express.Router();
module.exports = function(app){

  router.post(
    '/',
      soldItem.sellItem
  );

app.use('/soldItem',router);
};
