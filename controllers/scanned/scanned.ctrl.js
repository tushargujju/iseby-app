 
var moment = require('moment');
const connection = require('../../config/database').connection;
const subscription = require("../subscription/subscription.ctrl");

const async = require("async");

var getAllScannedProducts = function (req, res, next) {
    /*
          Author : Vinay Jadhav
          INformation : This API will give you all scanned products . Also it's giving you one field isSubscribed .
          This field is checked for subscription plan of seller;
     */

    var user_id = req.params.user_id;
    var responseObj = {};
    /*var getScannedProductsQuery = " SELECT tsi.`id` , tsi.`product_code` ,tsi.`scanned_by` , prod.`created_at` ," +
        "prod.`modified_at` , prod.`name` , prod.`description`,prod.`seller_id`,prod.`quantity`," +
        "prod.`price`,prod.`barcode_color`, prod.`category_id` , prod.`sub_category`,seller_info.`shop_name` ,seller_info.`shop_address` , user.`name` as `seller_name` , " + "tsd.`expiry_date`, " + "(CASE `expiry_date` WHEN tsd.`expiry_date` < NOW() THEN 1 ELSE 0 END) as isSubscribed " + "FROM `tbl_scanned_items` tsi INNER JOIN `tbl_products` prod ON " + "tsi.`product_code` = prod.`product_code` INNER JOIN `tbl_subscription_details` tsd " + "ON tsd.`seller_id` = prod.`seller_id` INNER JOIN `tbl_seller_info` seller_info ON seller_info.`id` = prod.`seller_id` INNER JOIN `tbl_users` user ON user.`id` = seller_info.`user_id` WHERE tsi.`scanned_by`='" + user_id + "'";*/
		
		
	var getScannedProductsQuery =
		"SELECT `tbl_scanned_items`.`scanned_by`, `tbl_products`.`product_code`, `tbl_products`.`isActive`, `tbl_products`.`id`, `tbl_products`.`product_code`, `tbl_products`.`name`, `tbl_products`.`description`, `tbl_products`.`quantity`, `tbl_products`.`price`, `tbl_products`.`discount_per`, `tbl_products`.`seller_id`, `tbl_seller_info`.`user_id`, `tbl_seller_info`.`shop_name` FROM `tbl_scanned_items`, `tbl_products`, `tbl_seller_info` WHERE `tbl_scanned_items`.`scanned_by` = '" + user_id + "' AND `tbl_products`.`product_code` = `tbl_scanned_items`.`product_code` AND `tbl_products`.`isActive` = '1' AND `tbl_seller_info`.`user_id` = `tbl_products`.`seller_id`";
		
	//var getScannedProductsQuery = " SELECT * FROM tbl_scanned_items WHERE scanned_by='"+user_id+"'";


    connection.query(getScannedProductsQuery, function (error, results, fields) {
        if (!error) {
            if (results.length > 0) {
                //results= async.each(results , function (item) {
                results.forEach(function (item) {
                    var todaysDate = new Date();
                    var expiryDate = moment(item.expiry_date, 'YYYY-MM-DD HH:MM:SS');
                    var todaysDate = moment(todaysDate, 'YYYY-MM-DD HH:MM:SS');
                    var remainingDays = expiryDate.diff(todaysDate, 'days');
                    if (remainingDays > 0) {
                        item.isSubscribed = 1
                    }
                });
                responseObj['status'] = 1;
                responseObj['message'] = "Scanned items found",
                responseObj['data'] = results;
                res.status(200).json(responseObj);
            } else {
                responseObj['status'] = 0;
                responseObj['message'] = "No Scanned items found";
                responseObj['data'] = error;
                res.status(401).json(responseObj);
            }
        } else {
            responseObj['status'] = 0;
            responseObj['message'] = error;
            responseObj['data'] = [];
            res.status(402).json(responseObj);
        }

    });
}

var create = function (req, res, next) {

    console.log(req.body);

    var xBody = req.body;

    var product_code = xBody.product_code;
    var scanned_by = xBody.scanned_by;

    var created_at = Math.round((new Date()).getTime() / 1000);
    var modified_at = Math.round((new Date()).getTime() / 1000);

    responseObj = {};

    var scannedItemCheckQuery = "SELECT COUNT(*) AS COUNT FROM tbl_scanned_items WHERE product_code='" + product_code + "' AND scanned_by='" + scanned_by + "'";

    connection.query(scannedItemCheckQuery, function (error, results, fields) {

        if (error) throw error;
        console.log(results[0]['COUNT']);

        if (results[0]['COUNT'] == 0) {

            var query_add_scanneditem = "INSERT INTO tbl_scanned_items (product_code, scanned_by, created_at, modified_at) VALUES ('" + product_code + "', '" + scanned_by + "', '" + created_at + "', '" + modified_at + "')";

            connection.query(query_add_scanneditem, function (error, results, fields) {
                if (error) throw error;

                responseObj['status'] = 1;
                responseObj['message'] = "Scan item successfully added.";
                res.status(200).json(responseObj);

            });

        } else {
            responseObj['status'] = 0;
            responseObj['message'] = "Product already scanned.";
            res.status(400).json(responseObj);
        }


    });


}

module.exports = {

    getAllScannedProducts: getAllScannedProducts,
    create: create,

};
