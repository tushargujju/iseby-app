const scannedCtrl = require('./scanned.ctrl');
const express = require('express');

const router = express.Router();


module.exports = function(app){

  router.get(
    '/:user_id',
    scannedCtrl.getAllScannedProducts
  );

  router.post(
    '/',
    scannedCtrl.create
  );

app.use('/scanned',router);
};
