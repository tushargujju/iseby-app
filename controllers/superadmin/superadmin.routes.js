const superadmin = require('./superadmin.ctrl');
const express = require('express');

const router = express.Router();


module.exports = function(app){

  router.get(
    '/',
      superadmin.getAllSellerInformation
  );
    router.post(
        '/',
        superadmin.updateSellerStatus
    );

    router.get(
        '/details',
        superadmin.getAllSellerProductsWithSellerInformation
    );

    router.post (
        '/status',
        superadmin.updateProductStatus
    );

    router.post(
        '/su',
        superadmin.getAllSubscribedUsers
    );

    app.use('/superadmin',router);
};
