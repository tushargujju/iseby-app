const connection = require('../../config/database').connection;
const userCntrl = require("../users/user.ctrl");
const productControl = require("../product/product.ctrl");
const subscriptionControl = require("../subscription_details/subscription_details.ctrl");

var getAllSellerInformation = function(req, res, next){
    /*
        Author : Vinay Jadhav
        Information: THis API will fwtch all seller information which are active
     */
    var inputData = {userType : 2};
    userCntrl.getAllUsersByUserType(inputData , function (response) {
        if(response.status){
            res.status(200).json(response);
        }else{
            res.status(400).json(response);
        }
    });
}


var getAllSellerProductsWithSellerInformation = function (req , resp) {
    productControl.getAllProductWithAllInformation(function (response) {
        if (response.status){
            resp.status(200).json(response);
        }else{
            resp.status(400).json(response);
        }
    });
}

var updateProductStatus = function (req , resp) {
    var inputData = {isActive : req.body.isActive, productId : req.body.productId};

    productControl.deactivateProduct(inputData , function (response) {
        if (response.status){
            resp.status(200).json(response);
        }else{
            resp.status(400).json(response);
        }
    });
}

var getAllSubscribedUsers = function (req , resp) {

    subscriptionControl.getAllSubscriptionDetailsWithUserInformation(function (response) {
        if (response.status){
            resp.status(200).json(response);
        }else{
            resp.status(400).json(response);
        }
    });
}

var updateSellerStatus = function(req, res, next){

    /*
        Author : Vinay Jadhav
        Information : This API will change status of a user;
     */

    var isActive = req.body.isActive;
    var userId =  req.body.id;
    var inputData = {isActive : isActive , id: userId };
    userCntrl.deleteUser(inputData , function (response) {
        if(response.status){
            res.status(200).json(response);
        }else{
            res.status(400).json(response);
        }
    });
};


  var create = function(req, res, next){

    console.log(req.body);

    var xBody = req.body;

    var name = xBody.name;
    var created_at = Math.round((new Date()).getTime() / 1000);
    var modified_at = Math.round((new Date()).getTime() / 1000);

    responseObj = {};

    connection.query("SELECT COUNT(*) AS COUNT FROM tbl_categories WHERE name='"+name+"'", function (error, results, fields) {

      if (error) throw error;
      console.log(results[0]['COUNT']);

      if(results[0]['COUNT'] == 0){

        var query_add_cat = "INSERT INTO tbl_categories (name, created_at, modified_at) VALUES ('"+name+"', '"+created_at+"', '"+modified_at+"')";

        connection.query(query_add_cat, function (error, results, fields) {
            if (error) throw error;

            responseObj['status']=1;
            responseObj['message']="Category added successfully.";
            res.status(200).json(responseObj);

        });



      }else{
        responseObj['status']=0;
        responseObj['message']="Category already exists.";
        res.status(400).json(responseObj);
      }


    });


}




module.exports = {
    getAllSellerInformation:getAllSellerInformation,
    updateSellerStatus:updateSellerStatus,
    getAllSellerProductsWithSellerInformation:getAllSellerProductsWithSellerInformation,
    updateProductStatus:updateProductStatus,
    getAllSubscribedUsers:getAllSubscribedUsers
};
