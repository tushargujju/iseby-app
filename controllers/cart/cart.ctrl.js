const connection = require('../../config/database').connection;
var dateFormat = require('dateformat');

var addToCart = function(req, res, next) {
    /*
                      Author : Vinay Jadhav,
                      Description : It will add a perticular product to cart
                      Return : Will return an cart_id
                   */
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };

    var buyerId = req.body.buyer_id;
    var product_id = req.body.product_id;
	
	var created_at = Math.round((new Date()).getTime() / 1000);
    var modified_at = Math.round((new Date()).getTime() / 1000);
    var created_on=dateFormat(new Date(), "yyyy-mm-dd h:MM:ss");

    if (buyerId != null && buyerId > 0) {
        if (product_id != null && product_id > 0) {
                if (created_at != null && created_at > 0) {
                    if (modified_at != null && modified_at > 0) {
                        //var insertValues = [buyerId, product_id, isbought, created_by, modified_by, created_on];
                        //var query = "INSERT INTO `tbl_cart`(`buyerId`, `productid`, `isbought`, `created_by`, `modified_by`, `created_on`) VALUES (?)";
						
                        var query = "SELECT * FROM `tbl_cart` WHERE `buyerid`='" + buyerId + "' AND `productid`='" + product_id + "'";
                        connection.query(query, function(error, results, fields) {
                            if (!error) {
                                if (results.length > 0) {
                                    responseObj.status = 1,
                                    responseObj.message = "Alresdy present in cart",
                                    responseObj.data = results.insertId
                                } else {
									var insertQuery = "INSERT INTO `tbl_cart`(`buyerid`, `productid`, `isbought`, `created_by`, `modified_by`, `created_on`) VALUES ('"+ buyerId +"' , '" + product_id + "', '0', '" + created_at + "', '" + modified_at + "', '" + created_on + "')";
									connection.query(insertQuery, function(error, results, fields) {
										if (!error) {
											if (results.affectedRows > 0) {
												responseObj.status = 0,
												responseObj.message = "Added to cart",
												responseObj.data = results.insertId
											} else {
												responseObj.status = 1,
												responseObj.message = "Sorry data is not inserted",
												responseObj.data = []
											}
											//res.status(200).json(responseObj);
										} else {
											console.log(error);
											responseObj.message = error;
											res.status(401).json(responseObj);
										}
									});
                                }
                                res.status(200).json(responseObj);
                            } else {
                                responseObj.message = error;
                                res.status(401).json(responseObj);
                            }
                        });
                    } else {
                        responseObj.message = "Please specify created_by  ";
                        res.status(401).json(responseObj);
                    }
                } else {
                    responseObj.message = "Please specify created_by  ";
                    res.status(401).json(responseObj);
                }
        } else {
            responseObj.message = "Please specify product_id";
            res.status(401).json(responseObj);
        }
    } else {
        responseObj.message = "Please specify buyerId";
        res.status(401).json(responseObj);
    }
};

var deleteByProductIdAndProductId = function(req, res, next) {
    /*
                  Author : Vinay Jadhav,
                  Description : This will perticular cart item using buyerId & productId
                  Return : Will return no of rows affected
               */
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };
	
	
    var buyerId = req.body.buyer_id;
    var product_id = req.body.product_id;

    if (buyerId != null && buyerId > 0) {
        if (product_id != null && product_id > 0) {
            var deleteValues = [buyerId, product_id];
            console.log(deleteValues);
            var query = "DELETE FROM `tbl_cart` WHERE `buyerid`=" + buyerId + " AND `productid`=" + product_id;
            connection.query(query, [deleteValues], function(error, results, fields) {
                if (!error) {
                    if (results.affectedRows > 0) {
                        responseObj.status = 1,
                            responseObj.message = "Data deleted",
                            responseObj.data = results.affectedRows
                    } else {
                        responseObj.status = 0,
                            responseObj.message = "Sorry data is not deleted",
                            responseObj.data = []
                    }
                    res.status(200).json(responseObj);
                } else {
                    responseObj.message = error;
                    res.status(401).json(responseObj);
                }
            });
        } else {
            responseObj.message = "Please specify product_id";
            res.status(401).json(responseObj);
        }
    } else {
        responseObj.message = "Please specify buyerId";
        res.status(401).json(responseObj);
    }
}

/*var getAllProductByUserIdAndProductId = function(req, res, next) {
    
    /*
                  Author : Vinay Jadhav,
                  Description : This will perticular cart item using buyerId & productId
                  Return : Will return no of rows affected
               */
    /*var responseObj = {
        status: 0,
        message: "",
        data: []
    };
    var userId = req.body.userId;
    var productId = req.body.productId;

    if (productId != null && productId > 0) {
        if (userId != null && userId > 0) {
            var getValue = [userId , productId];
            console.log(getValue);
            var query = "SELECT user.`id` as `user_id`,user. `name`,user. `email`,user. `mobile`,user. `password`,user. `address`,user. `latitude`,user. `longitude`,user. `is_verified`,user. `is_logged_in`,user. `last_login`,user. `city_id`,user. `points_earned`,user. `is_seller`,user. `seller_info_id`,user. `fcm_token`,user. `created_at`,user. `modified_at`,user. `isActive`,user. `buyer_address` , cart.`id` as `cart_id`,cart. `buyerId`,cart. `productid`,cart. `isbought`,cart. `created_by`,cart. `modified_by`,cart. `created_on` , prod.`id` as `product_id` , prod. `product_code`,prod. `name`,prod. `description`,prod. `seller_id`,prod. `quantity`,prod. `price`,prod. `barcode_color`,prod. `category_id`,prod. `sub_category`,prod. `created_at`,prod. `modified_at`,prod. `isActive`,prod. `isShippingIncluded`,prod. `shippingCharges`,prod. `addGst` FROM `tbl_cart` cart INNER JOIN `tbl_products` prod ON prod.`id` = cart.`productid` INNER JOIN `tbl_users` user on cart.`buyerId` = user.`id`"
            connection.query(query,  function(error, results, fields) {
                if (!error) {
                    if (results.length > 0) {
                            responseObj.status = 1,
                            responseObj.message = "Data found",
                            responseObj.data = results 
                    } else {
                            responseObj.status = 0,
                            responseObj.message = "No cart items present for this user",
                            responseObj.data = []
                    }
                    res.status(200).json(responseObj);
                } else {
                    responseObj.message = error;
                    res.status(401).json(responseObj);
                }
            });
        } else {
            responseObj.message = "Please specify userId";
            res.status(401).json(responseObj);
        }
    } else {
        responseObj.message = "Please specify product_id";
        res.status(401).json(responseObj);
    }

}*/


var getAllProductByUserId = function(req, res, next) {
    console.log("Hello Vinay");
    /*
                  Author : Vinay Jadhav,
                  Description : This will perticular cart item using buyerId & productId
                  Return : Will return no of rows affected
               */
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };
	
	var userId = req.params.user_id;;
  
        if (userId != null && userId > 0) {
            var getValue = [userId];
            //console.log(getValue);
            /*var query = "SELECT user.`id` as `user_id`,user. `name`,user. `email`,user. `mobile`,user. `password`,user. `address`,user. `latitude`,user. `longitude`,user. `is_verified`,user. `is_logged_in`,user. `last_login`,user. `city_id`,user. `points_earned`,user. `is_seller`,user. `seller_info_id`,user. `fcm_token`,user. `created_at`,user. `modified_at`,user. `isActive`,user. `buyer_address` , cart.`id` as `cart_id`,cart. `buyerId`,cart. `productid`,cart. `isbought`,cart. `created_by`,cart. `modified_by`,cart. `created_on` , prod.`id` as `product_id` , prod. `product_code`,prod. `name`,prod. `description`,prod. `seller_id`,prod. `quantity`,prod. `price`,prod. `barcode_color`,prod. `category_id`,prod. `sub_category`,prod. `created_at`,prod. `modified_at`,prod. `isActive`,prod. `isShippingIncluded`,prod. `shippingCharges`,prod. `addGst` FROM `tbl_cart` cart INNER JOIN `tbl_products` prod ON prod.`id` = cart.`productid` INNER JOIN `tbl_users` user on cart.`buyerId` = user.`id`";*/
			
			
			var query = "SELECT `tbl_cart`.`buyerid`, `tbl_products`.`id`, `tbl_products`.`product_code`, `tbl_products`.`name`, `tbl_products`.`price`, `tbl_products`.`discount_per`, `tbl_products`.`city_shipping`, `tbl_products`.`city_charges`, `tbl_products`.`out_charges`, `tbl_products`.`state_charges`, `tbl_products`.`buy_point`, `tbl_products`.`tax_per`, `tbl_products`.`tax_type`, `tbl_products`.`redeem_point`, `tbl_products`.`redeem_discount`, `tbl_seller_info`.`shop_name`, `tbl_seller_info`.`user_id`, `tbl_seller_info`.`city`, `tbl_seller_info`.`state`, `tbl_seller_info`.`pincode` FROM `tbl_cart`, `tbl_products`, `tbl_seller_info` WHERE `tbl_cart`.`buyerid` = '" + userId + "' AND `tbl_products`.`id` = `tbl_cart`.`productid` AND `tbl_seller_info`.`user_id` = `tbl_products`.`seller_id`";
			
            connection.query(query,  function(error, results, fields) {
                if (!error) {
					//console.log(results);
                    if (results.length > 0) {
                            responseObj.status = 1,
                            responseObj.message = "Data found",
                            responseObj.data = results 
                    } else {
                            responseObj.status = 0,
                            responseObj.message = "No cart items present for this user",
                            responseObj.data = []
                    }
                    res.status(200).json(responseObj);
                } else {
					console.log(error);
                    responseObj.message = error;
                    res.status(401).json(responseObj);
                }
            });
        } else {
            responseObj.message = "Please specify userId";
            res.status(401).json(responseObj);
        } 

}

module.exports = {
    addToCart: addToCart,
    deleteByProductIdAndProductId: deleteByProductIdAndProductId,
    //getAllProductByUserIdAndProductId:  getAllProductByUserIdAndProductId,
    getAllProductByUserId:  getAllProductByUserId
};