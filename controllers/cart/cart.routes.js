const cart = require('./cart.ctrl');
const express = require('express');

const router = express.Router();

module.exports = function(app){
  router.post(
    '/add',
      cart.addToCart
  );
   router.post(
    '/delete',
      cart.deleteByProductIdAndProductId
  );
    /*router.post(
    '/getAllProductByUserIdAndProductId',
      cart.getAllProductByUserIdAndProductId
  );*/
  router.get(
    '/:user_id',
      cart.getAllProductByUserId
  );
    app.use('/cart',router);
};
