const orderCtrl = require('./order.ctrl');
const express = require('express');

const router = express.Router();


module.exports = function(app){

  router.get(
    '/:user_id',
    orderCtrl.getAllOrders
  );
  
  router.get(
    '/seller/:user_id',
    orderCtrl.getAllOrderOfSeller
  );
  
  router.get(
    '/getAllSoldItemsOfSeller/:user_id',
    orderCtrl.getAllSoldItemsOfSeller
  );
  
  router.get(
    '/getAllOrderOfBuyer/:user_id',
    orderCtrl.getAllOrderOfBuyer
  );
  
  router.get(
    '/markAsShipped/:order_id',
    orderCtrl.markAsShipped
  );
  
  router.get(
    '/markAsOutForDelivery/:order_id',
    orderCtrl.markAsOutForDelivery
  );
  
  router.get(
    '/markAsDelivered/:order_id',
    orderCtrl.markAsDelivered
  );
  
  router.post(
    '/',
    orderCtrl.create
  );
  router.post(
	'/placeOrder',
	orderCtrl.placeOrder
  );
app.use('/order',router);
};
