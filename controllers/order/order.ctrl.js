const connection = require('../../config/database').connection;
const subscription = require("../subscription/subscription.ctrl");
const sellItems = require("../soldItems/sellItem.ctrl");

var dateFormat = require('dateformat');

var FCM = require('fcm-push');
//var proxyurl = 'http://192.168.1.10:5041';

var serverKey = 'AIzaSyBDtHAcMAZz9T20ULizD_YT_pvtVnMLlVQ';
var fcm = new FCM(serverKey );

var getAllOrders = function (req, res, next) {
    var responseObj = {};
    var user_id = req.params.user_id;
    subscription.checkSubscriptionOfUser({sellerId: user_id}, function (response) {
        if (response.status) {
            if (response.data.isExpired == 0) {
                var getproductsOfUserQuery = "SELECT bought.product_id, bought.buyer_id, user.`name`, user.`email`, user.`mobile`, user.`password`, user.`address`, user.`latitude`, user.`longitude`, user.`is_verified`, user.`is_logged_in`, user.`last_login`, user.`city_id`, user.`points_earned`, user.`is_seller`, user.`seller_info_id`, user.`fcm_token`, user.`created_at`, user.`modified_at`, user.`isActive`, user.`buyer_address`, prod.* FROM tbl_bought_products bought JOIN tbl_products prod ON bought.product_id= prod.id INNER JOIN `tbl_users` user ON user.id = bought.buyer_id WHERE bought.buyer_id = '" + user_id + "' AND prod.isActive='1'  GROUP BY bought.product_id";
				
				/*var getproductsOfUserQuery = "SELECT `tbl_bought_products`.`buyer_id`, `tbl_bought_products`.`product_id`, `tbl_products`.`id`, `tbl_products`.`name`, `tbl_products`.`product_code`, `tbl_products`.`description`, `tbl_products`.`quantity`, `tbl_products`.`price`, `tbl_products`.`discount_per`, `tbl_products`.`seller_id`, `tbl_products`.`category_id`, `tbl_products`.`sub_category`, `tbl_seller_info`.`user_id`, `tbl_seller_info`.`shop_name` FROM `tbl_bought_products`, `tbl_products`, `tbl_seller_info` WHERE `tbl_bought_products`.`buyer_id` = '" + user_id + "' AND `tbl_products`.`id` = `tbl_bought_products`.`product_id` AND `tbl_seller_info`.`user_id` = `tbl_products`.`seller_id`";*/
				
                connection.query(getproductsOfUserQuery, function (error, results, fields) {
                    if (error) throw error;
                    responseObj['status'] = 1;
                    responseObj['message'] = 'Bought products fetched successfully';
                    responseObj['data'] = results;
                    res.status(200).json(responseObj);
                });
            } else {
                responseObj['status'] = 0;
                responseObj['product_id'] = 0;
                responseObj['message'] = response.message;
                res.status(200).json(responseObj);
            }
        } else {
            responseObj['status'] = 0;
            responseObj['product_id'] = 0;
            responseObj['message'] = response.message;
            res.status(401).json(responseObj);
        }
    });
}


var getAllOrderOfSeller = function(req, res, next) {
    /*
                  Author : Vinay Jadhav,
                  Description : This will perticular cart item using buyerId & productId
                  Return : Will return no of rows affected
               */
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };
	
	var userId = req.params.user_id;;
  
        if (userId != null && userId > 0) {
            var getValue = [userId];
            //console.log(getValue);
            /*var query = "SELECT user.`id` as `user_id`,user. `name`,user. `email`,user. `mobile`,user. `password`,user. `address`,user. `latitude`,user. `longitude`,user. `is_verified`,user. `is_logged_in`,user. `last_login`,user. `city_id`,user. `points_earned`,user. `is_seller`,user. `seller_info_id`,user. `fcm_token`,user. `created_at`,user. `modified_at`,user. `isActive`,user. `buyer_address` , cart.`id` as `cart_id`,cart. `buyerId`,cart. `productid`,cart. `isbought`,cart. `created_by`,cart. `modified_by`,cart. `created_on` , prod.`id` as `product_id` , prod. `product_code`,prod. `name`,prod. `description`,prod. `seller_id`,prod. `quantity`,prod. `price`,prod. `barcode_color`,prod. `category_id`,prod. `sub_category`,prod. `created_at`,prod. `modified_at`,prod. `isActive`,prod. `isShippingIncluded`,prod. `shippingCharges`,prod. `addGst` FROM `tbl_cart` cart INNER JOIN `tbl_products` prod ON prod.`id` = cart.`productid` INNER JOIN `tbl_users` user on cart.`buyerId` = user.`id`";*/
			
			
			var query = "SELECT `tbl_transaction_products`.`prod_id` AS `product_id`, `tbl_transaction_products`.`prod_name`, `tbl_transaction_products`.`prod_code`, `tbl_transaction_products`.`price`, `tbl_transaction_products`.`discount_per`, `tbl_transaction_products`.`total`, `tbl_transaction_buyer`.`trans_date`, `tbl_transaction_buyer`.`id` as trans_id, `tbl_transaction_buyer`.`address`, `tbl_users`.`name`, `tbl_users`.`mobile`, `tbl_transaction_buyer`.`status`,  tbl_media.file_name FROM `tbl_transaction_products`, `tbl_transaction_buyer`, `tbl_users`, tbl_media  WHERE `tbl_transaction_products`.`seller_id` = '" + userId + "' AND `tbl_transaction_buyer`.`id` = `tbl_transaction_products`.`trans_id` AND `tbl_transaction_buyer`.`status` <= '2' AND `tbl_users`.`id` = `tbl_transaction_buyer`.`buyer_id` AND tbl_media.product_id=tbl_transaction_products.prod_id AND tbl_media.type=1 ORDER BY tbl_transaction_buyer.trans_date DESC";
			
            connection.query(query,  function(error, results, fields) {
                if (!error) {
					//console.log(results);
                    if (results.length > 0) {
                            responseObj.status = 1,
                            responseObj.message = "Data found",
                            responseObj.data = results 
                    } else {
                            responseObj.status = 0,
                            responseObj.message = "No cart items present for this user",
                            responseObj.data = []
                    }
                    res.status(200).json(responseObj);
                } else {
					console.log(error);
                    responseObj.message = error;
                    res.status(401).json(responseObj);
                }
            });
        } else {
            responseObj.message = "Please specify userId";
            res.status(401).json(responseObj);
        } 

}


var getAllOrderOfBuyer = function(req, res, next) {
    /*
                  Author : Vinay Jadhav,
                  Description : This will perticular cart item using buyerId & productId
                  Return : Will return no of rows affected
               */
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };
	
	var userId = req.params.user_id;;
  
        if (userId != null && userId > 0) {
            var getValue = [userId];
            //console.log(getValue);
            /*var query = "SELECT user.`id` as `user_id`,user. `name`,user. `email`,user. `mobile`,user. `password`,user. `address`,user. `latitude`,user. `longitude`,user. `is_verified`,user. `is_logged_in`,user. `last_login`,user. `city_id`,user. `points_earned`,user. `is_seller`,user. `seller_info_id`,user. `fcm_token`,user. `created_at`,user. `modified_at`,user. `isActive`,user. `buyer_address` , cart.`id` as `cart_id`,cart. `buyerId`,cart. `productid`,cart. `isbought`,cart. `created_by`,cart. `modified_by`,cart. `created_on` , prod.`id` as `product_id` , prod. `product_code`,prod. `name`,prod. `description`,prod. `seller_id`,prod. `quantity`,prod. `price`,prod. `barcode_color`,prod. `category_id`,prod. `sub_category`,prod. `created_at`,prod. `modified_at`,prod. `isActive`,prod. `isShippingIncluded`,prod. `shippingCharges`,prod. `addGst` FROM `tbl_cart` cart INNER JOIN `tbl_products` prod ON prod.`id` = cart.`productid` INNER JOIN `tbl_users` user on cart.`buyerId` = user.`id`";*/
			
			
			var query = "SELECT `tbl_transaction_products`.`prod_id` AS `product_id`, `tbl_transaction_products`.`prod_name`, `tbl_transaction_products`.`prod_code`, `tbl_transaction_products`.`price`, `tbl_transaction_products`.`discount_per`, `tbl_transaction_products`.`total`, `tbl_transaction_buyer`.`trans_date`, `tbl_transaction_buyer`.`id` as trans_id, `tbl_transaction_buyer`.`address`, `tbl_users`.`name`, `tbl_users`.`mobile`, `tbl_transaction_buyer`.`status`,  tbl_media.file_name FROM `tbl_transaction_products`, `tbl_transaction_buyer`, `tbl_users`, tbl_media  WHERE `tbl_transaction_products`.`seller_id` = '" + userId + "' AND `tbl_transaction_buyer`.`id` = `tbl_transaction_products`.`trans_id` AND `tbl_users`.`id` = `tbl_transaction_buyer`.`buyer_id` AND tbl_media.product_id=tbl_transaction_products.prod_id AND tbl_media.type=1 ORDER BY tbl_transaction_buyer.trans_date DESC";
			
            connection.query(query,  function(error, results, fields) {
                if (!error) {
					//console.log(results);
                    if (results.length > 0) {
                            responseObj.status = 1,
                            responseObj.message = "Data found",
                            responseObj.data = results 
                    } else {
                            responseObj.status = 0,
                            responseObj.message = "No cart items present for this user",
                            responseObj.data = []
                    }
                    res.status(200).json(responseObj);
                } else {
					console.log(error);
                    responseObj.message = error;
                    res.status(401).json(responseObj);
                }
            });
        } else {
            responseObj.message = "Please specify userId";
            res.status(401).json(responseObj);
        } 

}


var getAllSoldItemsOfSeller = function(req, res, next) {
    /*
                  Author : Vinay Jadhav,
                  Description : This will perticular cart item using buyerId & productId
                  Return : Will return no of rows affected
               */
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };
	
	var userId = req.params.user_id;;
  
        if (userId != null && userId > 0) {
            var getValue = [userId];
            //console.log(getValue);
            /*var query = "SELECT user.`id` as `user_id`,user. `name`,user. `email`,user. `mobile`,user. `password`,user. `address`,user. `latitude`,user. `longitude`,user. `is_verified`,user. `is_logged_in`,user. `last_login`,user. `city_id`,user. `points_earned`,user. `is_seller`,user. `seller_info_id`,user. `fcm_token`,user. `created_at`,user. `modified_at`,user. `isActive`,user. `buyer_address` , cart.`id` as `cart_id`,cart. `buyerId`,cart. `productid`,cart. `isbought`,cart. `created_by`,cart. `modified_by`,cart. `created_on` , prod.`id` as `product_id` , prod. `product_code`,prod. `name`,prod. `description`,prod. `seller_id`,prod. `quantity`,prod. `price`,prod. `barcode_color`,prod. `category_id`,prod. `sub_category`,prod. `created_at`,prod. `modified_at`,prod. `isActive`,prod. `isShippingIncluded`,prod. `shippingCharges`,prod. `addGst` FROM `tbl_cart` cart INNER JOIN `tbl_products` prod ON prod.`id` = cart.`productid` INNER JOIN `tbl_users` user on cart.`buyerId` = user.`id`";*/
			
			
			var query = "SELECT `tbl_transaction_products`.`prod_id` AS `product_id`, `tbl_transaction_products`.`prod_name`, `tbl_transaction_products`.`prod_code`, `tbl_transaction_products`.`price`, `tbl_transaction_products`.`discount_per`, `tbl_transaction_products`.`total`, `tbl_transaction_buyer`.`trans_date`, `tbl_transaction_buyer`.`id` as trans_id, `tbl_transaction_buyer`.`address`, `tbl_users`.`name`, `tbl_users`.`mobile`, `tbl_transaction_buyer`.`status`,  tbl_media.file_name FROM `tbl_transaction_products`, `tbl_transaction_buyer`, `tbl_users`, tbl_media  WHERE `tbl_transaction_products`.`seller_id` = '" + userId + "' AND `tbl_transaction_buyer`.`id` = `tbl_transaction_products`.`trans_id` AND `tbl_transaction_buyer`.`status` > '2' AND `tbl_users`.`id` = `tbl_transaction_buyer`.`buyer_id` AND tbl_media.product_id=tbl_transaction_products.prod_id AND tbl_media.type=1 ORDER BY tbl_transaction_buyer.trans_date DESC";
			
            connection.query(query,  function(error, results, fields) {
                if (!error) {
					//console.log(results);
                    if (results.length > 0) {
                            responseObj.status = 1,
                            responseObj.message = "Data found",
                            responseObj.data = results 
                    } else {
                            responseObj.status = 0,
                            responseObj.message = "No cart items present for this user",
                            responseObj.data = []
                    }
                    res.status(200).json(responseObj);
                } else {
					console.log(error);
                    responseObj.message = error;
                    res.status(401).json(responseObj);
                }
            });
        } else {
            responseObj.message = "Please specify userId";
            res.status(401).json(responseObj);
        } 

}



var create = function (req, res, next) {
    var xBody = req.body;

    var product_id = xBody.product_id;
    var buyer_id = xBody.buyer_id;
	var date=dateFormat(new Date(), "yyyy-mm-dd h:MM:ss");
    var trans_id = xBody.trans_id;
    var name = xBody.name;
    var product_code = xBody.product_code;	
    var seller_id1 = xBody.seller_id;	
    var price = xBody.price;
    var discount = xBody.discount_per;
    var quantity = xBody.quantity;
    var total = xBody.total;
    var buy_points = xBody.buy_points;
	
    var created_at = Math.round((new Date()).getTime() / 1000);
    var modified_at = Math.round((new Date()).getTime() / 1000);

    var responseObj = {};

	var query_add_transaction_product = "INSERT INTO tbl_transaction_products (trans_id, trans_date, prod_id, prod_name, prod_code, seller_id, price, discount_per, quantity, total, buy_points) VALUES ('" + trans_id + "', '" + date + "', '" + product_id + "', '" + name + "' , '" + product_code + "' , '" + seller_id1 + "' , '" + price + "' , '" + discount + "' , '" + quantity + "' , '" + total + "' , '" + buy_points + "' )";
	
	connection.query(query_add_transaction_product, function (error, results, fields) {
    if (!error){
		/*console.log(results);
        responseObj['status'] = 1;
		responseObj['message'] = "Inserted";
		responseObj['data'] = results;
		res.status(200).json(responseObj);*/
	
	
	
    var query_add_order = "INSERT INTO tbl_bought_products (product_id, buyer_id, created_at, modified_at)" +
        " VALUES ('" + product_id + "', '" + buyer_id + "', '" + created_at + "', '" + modified_at + "')";
    connection.query(query_add_order, function (error, results, fields) {
        if (!error){
            if(results.affectedRows > 0){

                //Get Product and Seller Info
                var getProductInfoQuery = "SELECT * FROM tbl_products WHERE id=" + product_id;
                connection.query(getProductInfoQuery, function (error, results, fields) {
                    if (results.length > 0) {
                        var mProduct = results[0];
                        var seller_id = mProduct.seller_id;
                        var buy_point = mProduct.buy_point;
						
                        var getSellerInfoQuery = "SELECT * FROM tbl_users WHERE id='" + mProduct.seller_id + "'";
                        connection.query(getSellerInfoQuery, function (error, userResults, fields) {
                            if(userResults.length > 0){
                                var sellerNotificationData = {};
                                var mSeller = userResults[0];
                                connection.query("SELECT * FROM tbl_users WHERE id = '" + buyer_id + "'", function (error, results, fields) {
                                    if(results.length > 0){
                                        var buyerData = results[0];
                                        var userPoints = parseInt(results[0]['points_earned']);
                                        userPoints = userPoints + buy_point;
                                        var query_update_points = "UPDATE tbl_users SET points_earned = '" + userPoints +
                                            "' WHERE id='" + buyer_id + "'";
                                        connection.query(query_update_points, function (error, results, fields) {
                                            if(results.affectedRows  > 0){
                                                var inputData = {buyer_id: buyer_id, seller_id: seller_id, productId: product_id};
                                                sellItems.sellItemFunction(inputData, function (response) {
                                                    if (response.status) {

                                                        sellerNotificationData['title'] = mProduct.name + " , has been purchased";
                                                        sellerNotificationData['message'] = mProduct.id + "has placed with code "+
                                                            mProduct.product_code;
                                                        sellerNotificationData['buyer_info'] = buyerData;
                                                        sellerNotificationData['product_info'] = mProduct;

                                                        var sellerNotificationObject = {
                                                            to: mSeller.fcm_token,
                                                            // data: {
                                                            //     info: sellerNotificationData
                                                            // },
                                                            notification: {
                                                                title:  mProduct.name + " , has been purchased",
                                                                body: mProduct.id + " , has placed with code "+ mProduct.product_code
                                                            }
                                                        };

                                                        console.log(mSeller.fcm_token);

                                                        fcm.send(sellerNotificationObject , function (err, response) {
                                                            console.log("FCM" , err , "Resp" , response);
                                                            if (err) {
                                                                console.log("Something has gone wrong! " + err);
                                                            } else {
                                                                console.log("Seller notification sent");
                                                                var buyerNotificationData= {};
                                                                buyerNotificationData['title'] = "Congrats !! Your order is placed";
                                                                buyerNotificationData['message'] = "Congrats !! You have purchased "
                                                                    + mProduct.name;
                                                                buyerNotificationData['seller_info'] = mSeller;
                                                                buyerNotificationData['product_info'] = mProduct;

                                                                var buyerNotificationObject = {
                                                                    to: buyerData['fcm_token'],
                                                                    data: {
                                                                        info: buyerNotificationData
                                                                    },
                                                                    notification: {
                                                                        title: "Congrats !! Your order is placed",
                                                                        body:  "Congrats !! You have purchased " +
                                                                        mProduct.name+". With unique id as "
                                                                        +mProduct.id + " & code is , "+ mProduct.code
                                                                    }
                                                                };

                                                                fcm.send(buyerNotificationObject, function(buyerNOtificationError ,
                                                                                                           buyerNotificationResp){
                                                                    if(!buyerNOtificationError ){
                                                                        console.log("buyer notifiaction sent");
                                                                    }else{
                                                                        console.log("No notification sent for buye");
                                                                    }
                                                                });
                                                            }
                                                        });
                                                        responseObj['status'] = 1;
                                                        responseObj['message'] = "Order generated successfully00000000000";
                                                        res.status(200).json(responseObj);
                                                    } else {
                                                        responseObj['status'] = 0;
                                                        responseObj['message'] = response.message;
                                                        res.status(401).json(responseObj);
                                                    }
                                                });
                                            }else{
                                                responseObj['status'] = 0;
                                                responseObj['message'] = "Error while updating an information of this buyer->" + buyer_id;
                                                res.status(200).json(responseObj);
                                            }
                                        });
                                    }else{
                                        responseObj['status'] = 0;
                                        responseObj['message'] = "No user information found for this buyer_id ->" + buyer_id;
                                        res.status(200).json(responseObj);
                                    }
                                });
                            }else{
                                responseObj['status'] = 0;
                                responseObj['message'] = "No user information found for this seller_id ->" + seller_id;
                                res.status(200).json(responseObj);
                            }
                        });
                    } else {
                        responseObj['status'] = 0;
                        responseObj['message'] = "Sorry this product doesn't exists.";
                        res.status(200).json(responseObj);
                    }

                });
            }else{
                responseObj['status'] = 0;
                responseObj['message'] = "Error while inserting values into tbl_bought_products";
                res.status(200).json(responseObj);
            }
        }else{
            responseObj['status'] = 0;
            responseObj['message'] = error;
            res.status(200).json(responseObj);
        }

    });
	
	}
	else {
		console.log(error);
		responseObj['status'] = 0;
		responseObj['product_id'] = 0;
		responseObj['message'] = "Not Inserted";
		res.status(200).json(responseObj);
	}
    });
	
}
	
	
	
var placeOrder = function (req, res, next) {
    var responseObj = {};
	var xBody = req.body;
    var user_id = xBody.buyer_id;
	var date=dateFormat(new Date(), "yyyy-mm-dd h:MM:ss");
    var sub_total = xBody.sub_total;
    var discount = xBody.discount;
    var tax_total = xBody.tax_total;
    var delivery_charges = xBody.delivery_charges;
    var transaction_amount = xBody.transaction_amount;
    var status1 = 0;
    var buy_points = xBody.buy_points;
    var redeem_point = xBody.redeem_points;
    var address = xBody.address;
	
    
    var insertTranscation = "INSERT INTO tbl_transaction_buyer ( buyer_id, trans_date, sub_total, discount, tax_total,delivery_charges, trans_amount, status, buy_points, redeem_point, address) VALUES ('" + user_id + "', '" + date + "', '" + sub_total + "', '" + discount + "', '" +  tax_total+ "', '" + delivery_charges + "', '" + transaction_amount + "', '0','" + buy_points + "' , '" + redeem_point + "', '" + address + "')";
    
	connection.query(insertTranscation, function (error, results, fields) {
    if (!error){
		console.log(results.insertId);
		
		connection.query("SELECT * FROM tbl_users WHERE id = '" + user_id + "'", function (error, results, fields) {
		if(results.length > 0){
			var buyerData = results[0];
			var userPoints = parseInt(results[0]['points_earned']);
			userPoints = userPoints - redeem_point;
			var query_update_points = "UPDATE tbl_users SET points_earned = '" + userPoints +
				"' WHERE id='" + user_id + "'";
			connection.query(query_update_points, function (error, results, fields) {
				if(results.affectedRows  > 0){
					
				}
			});
			}
		});					
        responseObj['status'] = 1;
		responseObj['message'] = results.insertId;
		responseObj['data'] = results.insertId;
		
		res.status(200).json(responseObj);
	}
	else {
		responseObj['status'] = 0;
		responseObj['product_id'] = 0;
		responseObj['message'] = response.message;
		res.status(200).json(responseObj);
	}
    });
}

var markAsShipped = function(req, res, next) {
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };
	
	var order_id = req.params.order_id;;
  
	if (order_id != null && order_id > 0) {
		var getValue = [order_id];
		var query = "UPDATE tbl_transaction_buyer SET status='1' WHERE id='" + order_id + "'";
		
		connection.query(query,  function(error, results, fields) {
			if (!error) {   
				
				responseObj.status = 1,
				responseObj.message = "Updated",
				responseObj.data = results 
				res.status(200).json(responseObj);
			}
			else {
				console.log(error);
				responseObj.status = 0,
				responseObj.message = "Error",
				res.status(401).json(responseObj);
			}
		});
	} else {
		responseObj.message = "Please specify userId";
		res.status(401).json(responseObj);
	} 
}


var markAsOutForDelivery = function(req, res, next) {
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };
	
	var order_id = req.params.order_id;;
  
	if (order_id != null && order_id > 0) {
		var getValue = [order_id];			
		var query = "UPDATE tbl_transaction_buyer SET status='2' WHERE id='" + order_id + "'";
		
		connection.query(query,  function(error, results, fields) {
			if (!error) {
				                 
				responseObj.status = 1,
				responseObj.message = "Updated",
				responseObj.data = results 
				res.status(200).json(responseObj);
			}
			else {
				console.log(error);
				responseObj.status = 0,
				responseObj.message = "Error",
				res.status(401).json(responseObj);
			}
		});
	} else {
		responseObj.message = "Please specify userId";
		res.status(401).json(responseObj);
	} 
}



var markAsDelivered = function(req, res, next) {
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };
	
	var order_id = req.params.order_id;;
  
	if (order_id != null && order_id > 0) {
		var getValue = [order_id];			
		var query = "UPDATE tbl_transaction_buyer SET status='3' WHERE id='" + order_id + "'";
		
		connection.query(query,  function(error, results, fields) {
			if (!error) {
				                 
				responseObj.status = 1,
				responseObj.message = "Updated",
				responseObj.data = results 
				res.status(200).json(responseObj);
			}
			else {
				console.log(error);
				responseObj.status = 0,
				responseObj.message = "Error",
				res.status(401).json(responseObj);
			}
		});
	} else {
		responseObj.message = "Please specify userId";
		res.status(401).json(responseObj);
	} 
}

module.exports = {

    getAllOrders: getAllOrders,
    create: create,
	getAllOrderOfSeller: getAllOrderOfSeller,
	getAllOrderOfBuyer: getAllOrderOfBuyer,
	getAllSoldItemsOfSeller: getAllSoldItemsOfSeller,
	markAsShipped: markAsShipped,
	markAsOutForDelivery: markAsOutForDelivery,
	markAsDelivered: markAsDelivered,
	placeOrder: placeOrder

};
