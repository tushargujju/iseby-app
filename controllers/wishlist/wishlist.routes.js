const wishlist = require('./wishlist.ctrl');
const express = require('express');

const router = express.Router();

module.exports = function(app){

  router.get(
    '/:buyer_id',
      wishlist.getAllWishlistOfBuyer
  );
  router.post(
    '/',
      wishlist.createWishlist
  );
    router.delete(
        '/',
        wishlist.deleteWishlist
    );
    app.use('/wishlist',router);
};
