const connection = require('../../config/database').connection;
var createWishlist = function (req, res, next) {
    /*
                      Author : Vinay Jadhav,
                      Description : It will create a wishlist for a buyer
                      Return : Will return a wishlist id
                   */
    var responseObj = {
        status :  0 ,
        message : "",
        data : []
    };

    var product_id = req.body.product_id;
    var buyer_id = req.body.buyer_id;
    var isActive = req.body.isActive;
    var created_date = req.body.created_date;
    var updated_date = req.body.updated_date;
    var notes   =     req.body.notes

    if(product_id!=null && product_id > 0){
        if (buyer_id!=null &&  buyer_id>0){
            if (isActive !=null && isActive in [0,1]){
                created_date = updated_date = new Date();
                var insertValues = [product_id , buyer_id , isActive , created_date , updated_date,notes];
                var query = "INSERT INTO `tbl_wishlist`( `product_id`, `buyer_id`, `isActive`, `created_date`, " +
                    "`updated_date`,`notes` )" +
                    "VALUES (?)";
                connection.query(query, [insertValues] , function (error, results, fields) {
                    if (!error) {
                        if (results.affectedRows > 0) {
                            responseObj.status = 1,
                                responseObj.message = "Data inserted",
                                responseObj.data = results.insertId
                        } else {
                            responseObj.status = 0,
                                responseObj.message = "Sorry data is not inserted",
                                responseObj.data = []
                        }
                        res.status(200).json(responseObj);
                    } else {
                        responseObj.message = error;
                        res.status(401).json(responseObj);
                    }
                });
            }else{
                responseObj.message = "Please specify isActive from 0 & 1";
                res.status(401).json(responseObj);
            }
        }else{
            responseObj.message = "Please specify buyer_id";
            res.status(401).json(responseObj);
        }
    }else{
        responseObj.message = "Please specify product_id";
        res.status(401).json(responseObj);
    }
};
var getAllWishlistOfBuyer = function (req, res, next) {
    /*
           Author : Vinay Jadhav,
           Description : This will give you all wishlist information of perticular buyer
           Return : It will return an array of wishlist
        */
    var buyers_id = req.params.buyer_id;
    var query = "SELECT w.`wishlist_id`, w.`product_id`, w.`buyer_id`, w.`isActive`, w.`created_date`, w.`updated_date`," + " w.`notes`, p.`product_code` , p.`name` , p.`description` , p.`seller_id` , " +"p.`quantity`,p.`price`, p.`barcode_color`, p.`category_id`,p.`sub_category`,p.`created_at`, p.`modified_at` ,seller_info.`shop_name`,seller_info.`shop_address`, user.`name` as `seller_name`, (CASE w.`wishlist_id` WHEN w.`isActive` = 1 THEN 0 ELSE 0 END) as type  FROM " + " `tbl_wishlist` w INNER JOIN `tbl_products` p ON p.`id` = w.`product_id` INNER JOIN `tbl_seller_info` seller_info ON seller_info.`id` = p .`seller_id` INNER JOIN `tbl_users` user ON user.`id` = seller_info.`user_id` WHERE  w.`buyer_id`=? " + " AND p.`isActive`=?";
	
	

                /*"SELECT w.`wishlist_id`, w.`product_id`, w.`buyer_id`, w.`isActive`, w.`created_date`, w.`updated_date`," +
                " w.`notes`, p.`id` as `product_id`, p.`product_code` , p.`name` , p.`description` , p.`seller_id` , " +
                "p.`quantity`,p.`price`,p.`barcode_color`,p.`category_id`,p.`sub_category`,p.`created_at`, p.`modified_at` ,seller_info.`shop_name` , user.`name` as `seller_name`, (CASE w.`wishlist_id` WHEN w.`isActive` = 1 THEN 0 ELSE 0 END) as type  FROM " +
                " `tbl_wishlist` w INNER JOIN `tbl_products` p ON p.`id` = w.`product_id` INNER JOIN `tbl_seller_info` seller_info ON seller_info.`id` = p .`seller_id` INNER JOIN `tbl_users` user ON user.`id` = seller_info.`user_id` WHERE  w.`buyer_id`=? " +
                " AND w.`isActive`=?";*/

    connection.query(query,[buyers_id , 1] , function (error, results, fields) {
        var responseObj = {
            status: 0,
            message: "",
            data: []
        };
        if (!error) {
            if (results.length > 0) {
                    responseObj.status = 1,
                    responseObj.message = "Data found",
                    responseObj.data = results
            } else {
                    responseObj.status = 0;
                    responseObj.message = "No Records found",
                    responseObj.data = []
            }
            res.status(200).json(responseObj);
        } else {
            responseObj.message = error;
            res.status(401).json(responseObj);
        }
    });

}
var deleteWishlist = function (req, res, next) {
                /*
                      Author : Vinay Jadhav,
                      Description : This will soft delete wishlist of buyer
                      Return : It will a status as 1 if deleted else 0
                   */
    var wishlist_id = req.body.wishlist_id;
    var responseObject = {
        status :  0 ,
        message : "",
        data : []
    };
    if(wishlist_id!=null || wishlist_id > 0){
        var query = "UPDATE `tbl_wishlist` SET `isActive`= ? WHERE `wishlist_id`=?";
        connection.query(query, [0 , wishlist_id] , function (error, results, fields) {
            if (!error) {
                if (results.affectedRows > 0) {
                    responseObject.status = 1,
                        responseObject.message = "Data deletd",
                        responseObject.data = results
                } else {
                    responseObject.status = 0,
                        responseObject.message = results,
                        responseObject.data = []
                }
                res.status(200).json(responseObject);
            } else {
                responseObject.message = error
                res.status(401).json(responseObject);
            }
        });
    }else{
        responseObject.message = "wishlist_id  should not be null";
        res.status(401).json(responseObject);
    }
};

module.exports = {
    getAllWishlistOfBuyer: getAllWishlistOfBuyer,
    createWishlist  : createWishlist,
    deleteWishlist: deleteWishlist
};
