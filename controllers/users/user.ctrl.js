const connection = require('../../config/database').connection;

var getAllUsers = function(req, res, next) {
    connection.query("SELECT * FROM tbl_users", function(error, results, fields) {

        var responseObj = {};

        if (error) throw error;

        responseObj['status'] = 1;
        responseObj['message'] = "Users fetched successfully";
        responseObj['data'] = results;

        //callback(error, responseObj);
        res.status(200).json(responseObj);

    });

}

var updateAddressByUserId = function(req, res, next) {

    /*
        Author : Vinay Jadhav
        Description : THis API wil update buyer_address by userId
    */
    var buyer_address = req.body.buyer_address;
    var userId = req.body.userId;
    responseObj = {
        status: 0,
        message: "",
        data: null
    };
    if (buyer_address.length > 0) {
        if (userId != null && userId > 0 && userId != undefined) {
            var query = "UPDATE tbl_users SET buyer_address = ?  WHERE id = ?";
            connection.query(query, [buyer_address , userId ] , function(updateError, updateResp) {
                if (!updateError) {
                    if (updateResp.affectedRows > 0) {
                        responseObj.status = 1;
                        responseObj.message = "Records updated successfully";
                        responseObj.data = updateResp;
                        res.status(200).json(responseObj);
                    } else {
                        responseObj.status = 0;
                        responseObj.message = "Sorry no records updated";
                        responseObj.data = null;
                        res.status(400).json(responseObj);
                    }
                } else {
                    responseObj.status = 0;
                    responseObj.message = updateError;
                    responseObj.data = null;
                    res.status(400).json(responseObj);
                }
            });
        } else {
            responseObj.status = 0;
            responseObj.message = "Please specify userid";
            responseObj.data = null;
        }
    } else {
        responseObj.status = 0;
        responseObj.message = "Address should not be kept blank";
        responseObj.data = null;
    }


}

var getAllUsersByUserType = function(inputData, callback) {

    /*
        Author : VInay Jadhav
        Information  : This function will help you to get all user by their user type . USer type will be either 1 or 2
     */
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };

    var userType = inputData.userType;
    var getQuery = "SELECT * FROM tbl_users WHERE `is_seller` = " + userType + " AND isActive=1";
    connection.query(getQuery, [userType], function(error, results, fields) {
        if (!error) {
            if (results.length > 0) {
                responseObj['status'] = 1;
                responseObj['message'] = "Sellers are fetched";
                responseObj['data'] = results;
                callback(responseObj);
            } else {
                responseObj['status'] = 0;
                responseObj['message'] = "No seller found";
                responseObj['data'] = [];
                callback(responseObj);
            }
        } else {
            responseObj['status'] = 0;
            responseObj['message'] = error;
            responseObj['data'] = [];
            callback(responseObj);
        }
    });
}

var myUsers = function(req, res, next) {

    connection.query("SELECT * FROM tbl_users", function(error, results, fields) {

        var responseObj = {};

        //if(error)throw error;

        responseObj['status'] = 1;
        responseObj['message'] = "Users fetched successfully";
        responseObj['data'] = results;

        //callback(error, responseObj);
        res.status(200).json(responseObj);

    });

}

var myProfile = function(req, res, next) {

    var user_id = req.params.id;

    connection.query("SELECT COUNT(id) AS COUNT FROM tbl_users WHERE id='" + user_id + "'", function(error, results, fields) {

        var responseObj = {};

        if (error) throw error;
        console.log(results[0]['COUNT']);

        if (results[0]['COUNT'] == 1) {
            responseObj['status'] = 1;

            var query = "SELECT * FROM tbl_users WHERE id='" + user_id + "'";

            connection.query(query, function(error, results, fields) {

                if (error) throw error;


                responseObj['message'] = "Account details found successfully";

                responseObj['user'] = results[0];

                res.status(200).json(responseObj);

            });

        } else {
            responseObj['status'] = 0;
            responseObj['message'] = "Invalid User ID.";
            res.status(401).json(responseObj);
        }

    });


}

var create = function(req, res, next) {
    var xBody = req.body;
    var name = xBody.name;
    var email = xBody.email;
    var mobile = xBody.mobile;
    var password = xBody.password;
    var is_logged_in = xBody.is_logged_in;
    var fcm_token = xBody.fcm_token;
    var city_id = 1;
    var is_seller = 1;
    var last_login = Math.round((new Date()).getTime() / 1000);
    var created_at = Math.round((new Date()).getTime() / 1000);
    var modified_at = Math.round((new Date()).getTime() / 1000);

    connection.query("SELECT COUNT(email) AS COUNT FROM tbl_users WHERE email='" + email + "'", function(error, results, fields) {

        var responseObj = {};

        if (error) throw error;
        console.log(results[0]['COUNT']);

        if (results[0]['COUNT'] == 0) {
            responseObj['status'] = 1;

            var query = "INSERT INTO tbl_users (name, email, mobile, is_logged_in, created_at, " +
                "modified_at,password, is_seller, last_login, fcm_token) VALUES" +
                " ('" + name + "', '" + email + "', '" + mobile + "', '" + is_logged_in + "', '" +
                created_at + "', '" + modified_at + "','" + password + "','" + is_seller + "','" +
                last_login + "','" + fcm_token + "')";

            connection.query(query, function(error, results, fields) {

                if (error) throw error;

                var selectData = "SELECT * FROM tbl_users WHERE id='" + results.insertId + "'";

                connection.query(selectData, function(error, results, fields) {

                    responseObj['message'] = "Account created successfully";
                    //req.me=results[0];
                    //next();

                    responseObj['user'] = results[0];
                    res.status(200).json(responseObj);

                    //Send email to User
                    /*  emailService.doSendEmail(xBody,function(error, success){

                        if(error)throw error;
                        console.log(success);

                      });

                      */

                });


            });

        } else {
            responseObj['status'] = 0;
            responseObj['message'] = "You are already registered with us.";
            res.status(401).json(responseObj);
        }

    });


}

var update = function(req, res, next) {

    console.log("Update Body : " + req.body);

    var xBody = req.body;

    var user_id = xBody.id;
    var name = xBody.name;
    var email = xBody.email;
    var mobile = xBody.mobile;
    //var password = xBody.password;
    //    var is_logged_in = xBody.is_logged_in;
    var city_id = 1;
    //  var device_type = xBody.device_type;
    //  var password=xBody.password;
    //  var created_at = Math.round((new Date()).getTime() / 1000);
    var modified_at = Math.round((new Date()).getTime() / 1000);

    connection.query("SELECT COUNT(email) AS COUNT FROM tbl_users WHERE email='" + email + "'", function(error, results, fields) {

        var responseObj = {};

        if (error) throw error;
        console.log(results[0]['COUNT']);

        if (results[0]['COUNT'] != 0) {
            responseObj['status'] = 1;

            //  var query = "INSERT INTO tbl_users (name, email, mobile, is_logged_in, created_at, modified_at,password) VALUES ('"+name+"', '"+email+"', '"+mobile+"', '"+is_logged_in+"', '"+created_at+"', '"+modified_at+"','"+password+"')";
            var query_update = "UPDATE tbl_users SET name = '" + name + "', mobile = '" + mobile + "' WHERE id='" + user_id + "'";

            console.log(query_update);

            connection.query(query_update, function(error, results, fields) {

                if (error) throw error;

                var selectData = "SELECT * FROM tbl_users WHERE email='" + email + "'";

                connection.query(selectData, function(error, results, fields) {

                    responseObj['message'] = "Account updated successfully";
                    req.me = results[0];
                    next();

                    responseObj['user'] = results[0];
                    res.status(200).json(responseObj);

                    //Send email to User
                    /*  emailService.doSendEmail(xBody,function(error, success){

                        if(error)throw error;
                        console.log(success);

                      });

                      */

                });


            });

        } else {
            responseObj['status'] = 0;
            responseObj['message'] = "You are not yet registered with us.";
            res.status(401).json(responseObj);
        }

    });


}

var deleteUser = function(inputData, callback) {
    /*
      Author : VInay Jadhav
      Information  : This function will help you to update status of a user
   */
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };

    var isActive = inputData.isActive;
    var id = inputData.id;
    var inputData = [isActive, id];

    var updateQuery = "UPDATE `tbl_users` SET  `isActive`=" + isActive + " WHERE `id`=" + id;
    connection.query(updateQuery, function(error, results, fields) {
        if (!error) {
            if (results.affectedRows > 0) {
                responseObj['status'] = 1;
                responseObj['message'] = "User status has been modified";
                responseObj['data'] = results;
                callback(responseObj);
            } else {
                responseObj['status'] = 0;
                responseObj['message'] = "Sorry no records updated ";
                responseObj['data'] = [];
                callback(responseObj);
            }
        } else {
            responseObj['status'] = 0;
            responseObj['message'] = error;
            responseObj['data'] = [];
            callback(responseObj);
        }
    });
}

var login = function(req, res, next) {

    console.log("Request Body : "+req.body);

    var xBody = req.body;

    var userName = xBody.email;
    var fcm_token = xBody.fcm_token;
    //  var type = xBody.type;
    //  var fcm_token = xBody.fcm_token;
    //  var device_type = xBody.device_type;
    var password = xBody.password;
    console.log("SELECT COUNT(email) AS COUNT FROM tbl_users WHERE email='" +
        userName + "' OR `mobile` = '" + userName + "'");

    connection.query("SELECT COUNT(email) AS COUNT FROM tbl_users WHERE email='" +
        userName + "' OR `mobile` = '" + userName + "'",
        function(error, results, fields) {

            var responseObj = {};

            if (error) throw error;
                console.log(results[0]['COUNT']);
                console.log("ERROR : "+error);

            if (results[0]['COUNT'] == 1) {

                //var query = "SELECT * FROM tbl_users WHERE email='" + email + "' AND password='" + password + "'";
                var query = "SELECT * FROM `tbl_users` WHERE  (`email`='" + userName + "' OR `mobile`='" + userName + "')AND `password`='" + password + "'";
                connection.query(query, function(error, userData, fields) {

                    console.log(results);


                    if (error || userData.length == 0) {
                        responseObj['status'] = 0;
                        responseObj['message'] = "Incorrect username or password.";
                        res.status(401).json(responseObj);
                    } else {

                        /*  const claims = {  id: results[0].id,roles: [1] }
                          const EXPIRATION = req.app.locals.SECURE_STATUS_EXPIRATION;
                          claims.secureStatus = moment()
                            .add(EXPIRATION, 'seconds')
                            .toJSON();
                          const token = jwt.generate('access', claims)
                          results[0].token=token
                          */

                        //Update FCM Token

                        var query_update = "UPDATE tbl_users SET fcm_token = '" + fcm_token + "' WHERE email='" + userName + "' OR `mobile`= '" + userName + "'";

                        connection.query(query_update, function(error, results, fields) {
                            if (error) throw error;

                            var query = "SELECT * FROM tbl_users WHERE email='" + userName + "' AND password='" + password + "'";
                            connection.query(query, function(error, results, fields) {

                                if(!error){
                                    responseObj['status'] = 1;
                                    responseObj['message'] = "Logged in successfully";
                                    responseObj['data'] = userData[0];
                                    res.status(200).json(responseObj);

                                }else{
                                    responseObj['status'] = 0;
                                    responseObj['message'] = "Sorry error while updating a FCM token";
                                    responseObj['data'] = userData[0];
                                    res.status(200).json(responseObj);
                                }
                                                            
                            });
                        });
                    }

                });

            } else {
                responseObj['status'] = 0;
                responseObj['message'] = "You are not yet registered with us.";
                res.status(401).json(responseObj);
            }

        });

}

var seller_reg = function(req, res, next) {
    var xBody = req.body;
    var user_id = xBody.id;updateAddressByUserId
    var shop_name = xBody.shop_name;
    var shop_address = xBody.shop_address;
    var city = xBody.city;
    var state = xBody.state;
    var pin_code = xBody.pin_code;
    var latitude = xBody.latitude;
    var longitude = xBody.longitude;
    var fcm_token = xBody.fcm_token;
    console.log("-->", fcm_token);
    var is_subscribed = 1;
    var is_seller = 2;
    var modified_at = Math.round((new Date()).getTime() / 1000);
    var created_at = Math.round((new Date()).getTime() / 1000);

    connection.query("SELECT COUNT(*) AS COUNT FROM tbl_users WHERE id='" + user_id + "'", function(error, results, fields) {

        var responseObj = {};

        if (error) throw error;


        if (results[0]['COUNT'] != 0) {


            connection.query("SELECT COUNT(*) AS COUNT FROM tbl_seller_info WHERE user_id='" + user_id + "'", function(error, results, fields) {

                if (error) throw error;

                if (results[0]['COUNT'] != 0) {
                    responseObj['status'] = 0;
                    responseObj['message'] = "You are already registered as a Seller.";
                    res.status(401).json(responseObj);
                } else {
                    var add_seller_info_query = "INSERT INTO tbl_seller_info (shop_address, city, state, pincode, shop_name, is_subscribed, " +
                        "user_id, created_at, modified_at) VALUES ('" + shop_address + "', '" + city +"', '"+ state + "', '" + pin_code + "', '" + shop_name + "', '" +
                        is_subscribed + "', '" + user_id + "', '" + created_at + "', '" + modified_at + "' )";

                    connection.query(add_seller_info_query, function(error, results, fields) {

                        if (error) throw error;

                        var query_update_user = "UPDATE tbl_users SET seller_info_id = '" + results.insertId + "', " +
                            "is_seller = '" + is_seller + "', latitude = '" + latitude + "', longitude = '" +
                            longitude + "', fcm_token = '" + fcm_token + "'WHERE id='" + user_id + "'";

                        connection.query(query_update_user, function(error, results, fields) {
                            if (error) throw error;

                            var selectSellerAndUserData = "SELECT u.*, s.shop_address, s.shop_name, s.user_id, " +
                                "s.is_subscribed FROM tbl_users u JOIN tbl_seller_info s ON u.id=s.user_id WHERE " +
                                "user_id = '" + user_id + "'";
                            connection.query(selectSellerAndUserData, function(error, results, fields) {

                                responseObj['status'] = 1;
                                responseObj['message'] = "Seller registered successfully";
                                // req.me=results[0];
                                // next();

                                responseObj['data'] = results[0];
                                res.status(200).json(responseObj);

                                //Send email to User
                                /*  emailService.doSendEmail(xBody,function(error, success){

                                  if(error)throw error;
                                  console.log(success);

                                });

                                */

                            });

                        });


                    });
                }

            });

        } else {
            responseObj['status'] = 0;
            responseObj['message'] = "Invalid User ID.";
            res.status(401).json(responseObj);
        }


    });


}

var redeemPoints = function(req, resp, next) {
    var responseObj = {
        status: 0,
        message: "",
        data: []
    };

    var body = req.body;
    var user_id = body.user_id;
    var redeemPoints = body.redeemPoints;
    if (user_id != null && user_id > 0) {
        if (redeemPoints != null && redeemPoints > 0) {
            var sqlString = "UPDATE `tbl_users` SET `points_earned` = `points_earned`-" + redeemPoints + " WHERE `id`=" + user_id;
            connection.query(sqlString, function(uError, uResp) {
                if (!uError) {
                    if (uResp.affectedRows > 0) {
                        responseObj.status = 1;
                        responseObj.message = "Points are redemmed";
                        responseObj.data = uResp;

                        resp.status(200).json(responseObj);
                    } else {
                        responseObj.message = "Unable to update Redeem points";
                        resp.status(400).json(responseObj);
                    }
                } else {
                    responseObj.message = uError;
                    resp.status(400).json(responseObj);
                }
            });
        } else {
            responseObj.message = "redeemPoints should not be null or zero";
            resp.status(400).json(responseObj);
        }
    } else {
        responseObj.message = "user_id should not be null or 0";
        resp.status(400).json(responseObj);
    }
};

module.exports = {
    getAllUsers: getAllUsers,
    myUsers: myUsers,
    create: create,
    myProfile: myProfile,
    update: update,
    login: login,
    seller_reg: seller_reg,
    getAllUsersByUserType: getAllUsersByUserType,
    deleteUser: deleteUser,
    redeemPoints: redeemPoints,
    updateAddressByUserId: updateAddressByUserId
};