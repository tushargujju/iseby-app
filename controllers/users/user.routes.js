const userCtrl = require('./user.ctrl');
const express = require('express');

const router = express.Router();


module.exports = function(app){

  router.get(
    '/all',
    userCtrl.getAllUsers
  );

  router.get(
    '/',
    userCtrl.myUsers
  );

  router.post(
    '/',
    userCtrl.create
  );

  router.get(
    '/:id',
    userCtrl.myProfile
  );

  router.put(
    '/',
    userCtrl.update
  );

  router.post(
    '/login',
    userCtrl.login
  );

  router.put(
    '/seller_reg',
    userCtrl.seller_reg
  );

    router.put(
        '/redeem_points',
        userCtrl.redeemPoints
    );

    router.put(
        '/buyer_address',
        userCtrl.updateAddressByUserId
    );


app.use('/user',router);
};
