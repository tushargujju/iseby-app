const connection = require('../../config/database').connection;

var getAllSubCategories = function(req, res, next){

  connection.query("SELECT * FROM tbl_sub_categories", function(error, results, fields){

    var responseObj = {};

    //if(error)throw error;
    responseObj['status'] = 1;
    responseObj['message']='Sub Categories fetched successfully';
    responseObj['data']=results;

    res.status(200).json(responseObj);


  });

}

  var create = function(req, res, next){

    console.log(req.body);

    var xBody = req.body;

    var name = xBody.name;
    var category_id = xBody.category_id;
    var created_at = Math.round((new Date()).getTime() / 1000);
    var modified_at = Math.round((new Date()).getTime() / 1000);

    responseObj = {};

    connection.query("SELECT COUNT(*) AS COUNT FROM tbl_sub_categories WHERE name='"+name+"'", function (error, results, fields) {

      if (error) throw error;
      console.log(results[0]['COUNT']);

      if(results[0]['COUNT'] == 0){

        var query_add_cat = "INSERT INTO tbl_sub_categories (name, category_id, created_at, modified_at) VALUES ('"+name+"', '"+category_id+"', '"+created_at+"', '"+modified_at+"')";

        connection.query(query_add_cat, function (error, results, fields) {
            if (error) throw error;

            responseObj['status']=1;
            responseObj['message']="Sub Category added successfully.";
            res.status(200).json(responseObj);

        });



      }else{
        responseObj['status']=0;
        responseObj['message']="Sub Category already exists.";
        res.status(400).json(responseObj);
      }


    });


}




module.exports = {

  getAllSubCategories:getAllSubCategories,
  create:create,

};
