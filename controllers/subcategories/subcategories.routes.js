const subCatCtrl = require('./subcategories.ctrl');
const express = require('express');

const router = express.Router();


module.exports = function(app){

  router.get(
    '/',
    subCatCtrl.getAllSubCategories
  );

  router.post(
    '/',
    subCatCtrl.create
  );

app.use('/subcategories',router);
};
